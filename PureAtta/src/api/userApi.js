import appService from "../service/appService"

export const getCarDetails = async(_id) => {
    return new Promise((resolve, reject) => {
        return appService.get(`/mobile/cart/details/${_id}`).then((resp)=>{
            return resolve(resp?.data)
        }).catch((err=>{
            return reject(err)
        }))
    })
}

