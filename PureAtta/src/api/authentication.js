import authService from "../service/authService"


export const login = async(data) => {
    return new Promise((resolve, reject) => {
        return authService.post('/auth/login',data).then((resp)=>{
            return resolve(resp?.data)
        }).catch((err=>{
            return reject(err)
        }))
    })
}

export const verify = async(data) => {
    return new Promise((resolve, reject) => {
        return authService.post('/auth/verify',data).then((resp)=>{
            return resolve(resp?.data)
        }).catch((err=>{
            return reject(err)
        }))
    })
}