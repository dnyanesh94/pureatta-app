import appService from "../service/appService"

export const productPageData = async(productId) => {
    return new Promise((resolve, reject) => {
        return appService.get(`/mobile/product/${productId}`).then((resp)=>{
            return resolve(resp?.data)
        }).catch((err=>{
            console.log(err)
            return reject(err)
        }))
    })
}

export const validateVariant = async(data) => {
    return new Promise((resolve, reject) => {
        return appService.post(`/mobile/validate/variant`, data).then((resp)=>{
            return resolve(resp?.data)
        }).catch((err=>{
            console.log(err)
            return reject(err)
        }))
    })
}