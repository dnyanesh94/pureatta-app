import React from 'react';
import {View, StyleSheet, Text} from 'react-native';
import { getWidth } from '../utils/responsive';

const Header = (props) => {
    const {left, middle, right} = props;

    return(
        <View style={styles.container}>
            {
                left &&
                <View style={styles.left}>
                    {left()}
                </View>
            }
            {
                middle &&
                <View style={styles.middle}>
                    {middle()}
                </View>
            }
            {
                right &&
                <View style={styles.right}>
                    {right()}
                </View>
            }
        </View>
    )
}

const styles = StyleSheet.create({
    container:{
        flex: 1,
        flexDirection: 'row',
        height: "100%",
    },
    left:{
        flex:2,
        height: '100%',
        justifyContent: 'center',
        alignItems: 'flex-start',
    },
    middle:{
        flex:6,
        height: '100%',
        justifyContent: 'center',
        alignItems: 'center',
    },
    right:{
        flex:2,
        height: '100%',
        justifyContent: 'center',
        alignItems: 'flex-end',
    }

})

export default Header;