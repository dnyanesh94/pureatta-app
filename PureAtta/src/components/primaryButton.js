import React from 'react';
import { TouchableOpacity, StyleSheet, Text } from 'react-native';
import { fSize } from '../utils/responsive';
import { MEDIUM } from '../utils/typography';

const PrimaryButton = (props) => {
    const { title, onPress} = props;
    return(
        <TouchableOpacity style={styles.button} onPress={onPress}>
            <Text style={styles.buttonText}>
                {title}
            </Text>
        </TouchableOpacity>
    )
}

const styles = StyleSheet.create({
    button:{
        width: "100%",
        backgroundColor: "#F2921D",
        borderRadius: 100,
        justifyContent: 'center',
        alignItems: 'center',
        height: 48
    },
    buttonText:{
        fontFamily: MEDIUM,
        fontSize: fSize(14),
        lineHeight: 17,
        color: "#fff",
        textAlign: 'center'
    }
})

export default PrimaryButton