import React from 'react';
import {StyleSheet, View, Text} from 'react-native'
import Toast, { BaseToast, ErrorToast } from 'react-native-toast-message';
import { fSize, getHeight, getWidth } from '../utils/responsive';
import { REGULAR, SEMI_BOLD } from '../utils/typography';


const styles = StyleSheet.create({
    toastContainer:{
        height: getHeight(100),
        width: getWidth(100),
        justifyContent: 'center',
        alignItems: 'center'
    },
    toast:{
        padding: 10,
        backgroundColor: "#fff",
        borderRadius: 5,
    },
    messageText: {
        fontFamily: SEMI_BOLD,
        fontSize: fSize(12),
        color: "#000"
    }
})

const CustomToast = () => {
    const toastConfig = {
        success: (props) => (
          <BaseToast
            {...props}
            style={{ 
                backgroundColor: "#fff",
                borderWidth: 1,
                borderColor: 'green',//"rgb(242, 146, 29)",
                borderLeftWidth:1,
                borderLeftColor: 'green',//"rgb(242, 146, 29)",
                borderRadius: 12,
                width:'80%',
                
            }}
            // contentContainerStyle={{ paddingHorizontal: 15}}
            text1Style={{
              fontSize: fSize(12),
              fontFamily: REGULAR,
              color: 'green' ,//"rgb(242, 146, 29)",
              textAlign: 'center'
            }}
          />
        ),
        error: (props) => (
          <ErrorToast
            {...props}
            style={{ 
              backgroundColor: "#fff",
              borderWidth: 1,
              borderColor: 'red',//"rgb(242, 146, 29)",
              borderLeftWidth:1,
              borderLeftColor: 'red',//"rgb(242, 146, 29)",
              borderRadius: 12,
              width:'80%',
              
          }}
          // contentContainerStyle={{ paddingHorizontal: 15}}
          text1Style={{
            fontSize: fSize(12),
            fontFamily: REGULAR,
            color: 'red' ,//"rgb(242, 146, 29)",
            textAlign: 'center'
          }}
          />
        ),
        customToast: ({ text1,  props }) => (
          <View style={styles.toastContainer}>
            <View style={styles.toast}>
                <Text style={styles.messageText}>{text1}</Text>
                {/* <Text>{props.uuid}</Text> */}
            </View>
          </View>
        )
    };
    return (
        <Toast config={toastConfig} position={"top"} topOffset={getHeight(40)} />
    );
  
}
export default CustomToast
