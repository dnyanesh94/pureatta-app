/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React,{useEffect} from 'react';
import {StatusBar} from 'react-native';
import {Provider} from 'react-redux';

import Navigator from './navigation/navigator';
import {store} from '../redux'
import CustomToast from '../components/CustomToast';

const App = () => {
  
  useEffect(()=>{
    StatusBar.setHidden(true);
  },[])
  
  return(
    <Provider store={store}>
      <StatusBar  backgroundColor={'#17202D'} hidden={true} />
      <Navigator />
      <CustomToast />
    </Provider>
  ) 
}

export default App;
