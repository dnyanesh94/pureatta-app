import * as React from 'react';
import { createNativeStackNavigator } from '@react-navigation/native-stack';

import AsyncStorage from '@react-native-async-storage/async-storage';
import { useEffect } from 'react';
import { navigate, resetNavigation } from './navigator';

import { ScreenNames } from '../../utils/screens';
import Home from '../../pages/home';
import PhoneLogin from '../../pages/auth/phoneLoginScreen';
import EmailLogin from '../../pages/auth/emailLoginScreen';
import Otp from '../../pages/auth/otpScreen';
import AppHome from '../../pages/app/appHome';
import ProductDetails from '../../pages/app/productDetails.js/index.js';
import Cart from '../../pages/app/cart';
import CartSummary from '../../pages/app/cartSummary';
import ManageAddress from '../../pages/app/menu/manageAddress';
import MyProfile from '../../pages/app/menu/myProfile';
import MyOrder from '../../pages/app/menu/myOrder';
import OrderDetails from '../../pages/app/menu/myOrder/orderDetails';
import OrderSuccess from '../../pages/app/orderSuccess';

const Stack = createNativeStackNavigator();

export const Routes = () => {

    // useEffect(() => {
    //     if (token !== null && isRegistered !== null) {
    //         resetNavigation(ScreenNames.START_RIDE)
    //     }
    // }, [token, isRegistered])


    return (
        <Stack.Navigator
            headerMode="none"
            initialRouteName={ScreenNames.HOME}
            screenOptions={{
            headerShown: false
            }}
        >
        <Stack.Screen name={ScreenNames.HOME} component={Home} />
        <Stack.Screen name={ScreenNames.PHONE_LOGIN} component={PhoneLogin} />
        <Stack.Screen name={ScreenNames.EMAIL_LOGIN} component={EmailLogin} />
        <Stack.Screen name={ScreenNames.OTP} component={Otp} />
        <Stack.Screen name={ScreenNames.APP_HOME} component={AppHome} />
        <Stack.Screen name={ScreenNames.PRODUCT_DETAILS} component={ProductDetails} />
        <Stack.Screen name={ScreenNames.CART} component={Cart} />
        <Stack.Screen name={ScreenNames.CART_SUMMARY} component={CartSummary} />
        <Stack.Screen name={ScreenNames.MANAGE_ADDRESS} component={ManageAddress} />
        <Stack.Screen name={ScreenNames.MY_PROFILE} component={MyProfile} />
        <Stack.Screen name={ScreenNames.MY_ORDER} component={MyOrder} />
        <Stack.Screen name={ScreenNames.ORDER_DETAILS} component={OrderDetails} />
        <Stack.Screen name={ScreenNames.ORDER_SUCCESS} component={OrderSuccess} />
        
      </Stack.Navigator>
    )
}