import * as React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { Routes } from './routes';
import { navigationRef } from './index';

import AsyncStorage from '@react-native-async-storage/async-storage';

const Navigator = () => {
    React.useEffect(() => {

    }, []);

    return (
        <NavigationContainer ref={navigationRef}>
            <Routes/>
        </NavigationContainer>
      );
}
export default Navigator