import AsyncStorage from '@react-native-async-storage/async-storage';
import axios from "axios";
import { API_URL } from "../config";

class appService {
    constructor(){
        this.setInterceptor();
    }

    setInterceptor = () => {
        let service = axios.create({
            baseURL: API_URL,
            Accept: 'application/json',
            'Content-Type': 'application/json;multipart/form-data',
          });

          service.interceptors.request.use(
            async (config) => {
              let token = await AsyncStorage.getItem('token');             
              config.headers = { 
                'Authorization': `Bearer ${token}`,
                'Accept': '*/*',
                'Content-Type': 'application/json'
              }
              return config;
            },
            (error) => {
              return Promise.reject(error);
            },
          );

          service.interceptors.response.use(
            function (response) {
              return response;
            },
            async (error) => {
              const originalRequest = error.config;
              if (error?.response?.status === 500 && error?.response?.data?.message === "jwt expired") {
                await AsyncStorage.removeItem('token')
                let token = await AsyncStorage.getItem('token')
                if (!token) {
                  props.navigation.navigate(ScreenNames.HOME)
                }
              }
              // if (error?.response?.status === 422 && !originalRequest._retry) {
              //   originalRequest._retry = true;
              //   const data = await this.refreshAccessToken(); 
              //   await AsyncStorage.setItem('token', data.data.access_token);
              //   axios.defaults.headers.common['Authorization'] = 'Bearer ' + data.data.access_token;
              //   return service(originalRequest);
              // }
              return Promise.reject(error);
            },
          );
          this.service = service;
    }

    refreshAccessToken = async ()=>{
        let refershToken = await AsyncStorage.getItem('refreshToken'); 
        return axios.post(API_URL + '/refresh',{},{
          headers: {'Authorization':`Bearer ${refershToken}`}
        })
        .then((response) => this.successResponse(response))
        .catch((error) => this.errorResponse(error));
    }

    get(url) {
      return this.service.get(API_URL + url)
      .then((response) => this.successResponse(response))
      .catch((error) => this.errorResponse(error));
    }

    post(url,payload) {
      return this.service.post(API_URL + url, payload)
      .then((response) => this.successResponse(response))
      .catch((error) => this.errorResponse(error));
    }

    put(url,payload) {
      return this.service.put(API_URL + url, payload)
      .then((response) => this.successResponse(response))
      .catch((error) => this.errorResponse(error));
    }

    patch(url,payload) {
      return this.service.patch(API_URL + url, payload)
      .then((response) => this.successResponse(response))
      .catch((error) => this.errorResponse(error));
    }
    
    delete(url,payload) {
      return this.service.delete(API_URL + url, payload)
      .then((response) => this.successResponse(response))
      .catch((error) => this.errorResponse(error));
    }

    successResponse(response) {
        return Promise.resolve(response);
    }
    
    errorResponse(error) {
        return Promise.reject(error);
    }

}
export default new appService();