import axios from "axios";
import { API_URL } from "../config";

const header = {
    accept: 'application/json',
    'content-type': 'application/json',
};

const authService = {
    get(url) {
        return axios.get(API_URL + url, {
          headers: header,
        })
        .then((response) => this.successResponse(response))
        .catch((error) => this.errorResponse(error));
      },

      post(url,payload) {
        return axios.post(API_URL + url, payload, {
          headers: header,
        })
        .then((response) => this.successResponse(response))
        .catch((error) => this.errorResponse(error));
      },

      put(url,payload) {
        return axios.put(API_URL + url, payload, {
          headers: header,
        })
        .then((response) => this.successResponse(response))
        .catch((error) => this.errorResponse(error));
      },
      
      delete(url,payload) {
        return axios.delete(API_URL + url, payload, {
          headers: header,
        })
        .then((response) => this.successResponse(response))
        .catch((error) => this.errorResponse(error));
      },

      successResponse(response) {
        return Promise.resolve(response);
      },
    
      errorResponse(error) {
        return Promise.reject(error);
      }
    
}
export default authService;
