import { API_URL } from "../../config"


export const filepathExtractor = (url) => {
    return url ? API_URL+'/'+url?.split('\\').join("/") : ''
}