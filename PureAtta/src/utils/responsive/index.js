import { Dimensions, Platform, PixelRatio } from "react-native";

const {height, width} = Dimensions.get('window')
const scale = width / 320;

const getHeight = (ht) => {
    return (ht * (height/100))
}

const getWidth = (wd) => {
    return (wd * (width/100))
}

// based on iphone 5s's scale

const fSize = (size) => {
  const newSize = size * scale 
  if (Platform.OS === 'ios') {
    return Math.round(PixelRatio.roundToNearestPixel(newSize))
  } else {
    return Math.round(PixelRatio.roundToNearestPixel(newSize)) - 2
  }
}


export {getHeight, getWidth, fSize}