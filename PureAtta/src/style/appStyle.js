import { getHeight, getWidth } from "../utils/responsive";
import {StyleSheet} from 'react-native';

const  appStyle = StyleSheet.create({
    container:{
        width: getWidth(100),
        height: getHeight(100),
    },
    header:{
        width: getWidth(100),
        height: getHeight(10),
        paddingHorizontal: 20
    },
    content:{
        width: getWidth(100),
        height: getHeight(80),
        paddingHorizontal: 20
    },
    footer:{
        width: getWidth(100),
        height: getHeight(10),
        paddingHorizontal: 20
    }
})
export default appStyle;