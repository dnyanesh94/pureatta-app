const initialState = {
    data:null
};

const cartReducer = (state = initialState, {type, payload}) => {
    switch (type) {
        case 'SET_CART_RESPONSE':
            return {
                ...state,
                data:payload
            };
        default:
            return state;
    }
};

export default cartReducer;
