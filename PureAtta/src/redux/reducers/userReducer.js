const initialState = {
    data:null,
    addressList:[],
};

const userReducer = (state = initialState, {type, payload}) => {
    switch (type) {
        case 'SET_USER_DATA':
            return {
                ...state,
                data: payload
            };

        case 'SET_USER_ADDRESS_LIST':
            const data = payload?.sort((x,y)=> y.isDefault - x.isDefault)
            return {
                ...state,
                addressList: data,
            };    
        default:
            return state;
    }
};

export default userReducer;
