const initialState = {
    deliveryAddress:null,
    deliverySlots: [],
    selectedDeliverSlot:null,
};

const cartSummaryReducer = (state = initialState, {type, payload}) => {
    switch (type) {
        case 'SET_DELIVERY_ADDRESS':
            return {
                ...state,
                deliveryAddress:payload
            };

        case 'SET_DELIVERY_SLOTS':
            return {
                ...state,
                deliverySlots:payload
            };  
        case 'SET_SELECTED_DELIVERY_SLOT':
            return{
                ...state,
                selectedDeliverSlot:payload
            }      
        default:
            return state;
    }
};

export default cartSummaryReducer;
