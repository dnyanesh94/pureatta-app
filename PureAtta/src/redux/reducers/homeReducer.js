const initialState = {
    data:null
};

const homeReducer = (state = initialState, {type, payload}) => {
    switch (type) {
        case 'SET_HOME_DATA':
            return {
                ...state,
                data: payload
            };
        default:
            return state;
    }
};

export default homeReducer;
