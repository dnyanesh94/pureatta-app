import Toast from 'react-native-toast-message';
import { Vibration } from 'react-native';
import appService from "../../service/appService";

export function setHomeData(data){
    return{
        type: "SET_HOME_DATA",
        payload:data
    }
}


export function getHomeData () {
    return async (dispatch) => {
        return appService.get(`/mobile/homepage`).then((response)=>{
            if (response.status !== 200) {
                throw Error(response);
            }
            return response.data;
        }).then((response)=>{
            dispatch(setHomeData(response?.data))
        }).catch((err=>{
            console.log(err)
        }))
    }
}