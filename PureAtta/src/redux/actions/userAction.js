import Toast from 'react-native-toast-message';
import appService from "../../service/appService";
import { setDeliveryAddress } from './cartSummaryAction';

export function setUserData(data){
    return{
        type: "SET_USER_DATA",
        payload:data
    }
}

export function setUserAddressList(data){
    return{
        type: "SET_USER_ADDRESS_LIST",
        payload:data
    }
}

export function getUserData() {
    return async (dispatch) => {
        return appService.get(`/user`).then((response)=>{
            if (response.status !== 200) {
                throw Error(response);
            }
            return response.data;
        }).then((response)=>{
            dispatch(setUserData(response?.data))
        }).catch((err=>{
            console.log(err)
        }))
    }
}

export function updateUserData(formData) {
    return async (dispatch) => {
        return appService.post(`/user/edit`, formData).then((response)=>{
            if (response.status !== 200) {
                throw Error(response);
            }
            return response.data;
        }).then((response)=>{
            dispatch(setUserData(response?.data))
            Toast.show({
                type: 'success',
                text1: 'user updated successfully.'
            })
        }).catch((err=>{
            Toast.show({
                type: 'error',
                text1: 'failed to edit user'
            })
            console.log(err)
        }))
    }
}

export function getUserAddressList() {
    return async (dispatch) => {
        return appService.get(`/user/address/list`).then((response)=>{
            if (response.status !== 200) {
                throw Error(response);
            }
            return response.data;
        }).then((response)=>{
            dispatch(setUserAddressList(response?.data))
            let deliveryAddress = response?.data?.find(item=>item?.isDefault)
            dispatch(setDeliveryAddress(deliveryAddress))
        }).catch((err=>{
            dispatch(setUserAddressList([]))
            console.log(err)
        }))
    }
}

export function changeDefaultAddress(formData) {
    return async (dispatch) => {
        return appService.post(`/user/address/change/default`, formData).then((response)=>{
            if (response.status !== 200) {
                throw Error(response);
            }
            return response.data;
        }).then((response)=>{
            Toast.show({
                type: 'success',
                text1: 'Default address changed'
            })
            dispatch(getUserAddressList())
        }).catch((err=>{
            Toast.show({
                type: 'error',
                text1: 'Failed to change default'
            })
            console.log(err)
        }))
    }
}

export function addUserAddress(formData) {
    return async (dispatch) => {
        return appService.post(`/user/address/add`, formData).then((response)=>{
            if (response.status !== 200) {
                throw Error(response);
            }
            return response.data;
        }).then((response)=>{
            Toast.show({
                type: 'success',
                text1: 'User address added'
            })
            dispatch(getUserAddressList())
        }).catch((err=>{
            console.log(err)
            Toast.show({
                type: 'error',
                text1: 'Failed to add user address'
            })
        }))
    }
}