import Toast from 'react-native-toast-message';
import { Vibration } from 'react-native';
import appService from "../../service/appService";

export function setCartData(data){
    return{
        type: "SET_CART_RESPONSE",
        payload:data
    }
}


export function getCart () {
    return async (dispatch) => {
        return appService.get(`/cart/get`).then((response)=>{
            if (response.status !== 200) {
                throw Error(response);
            }
            return response.data;
        }).then((response)=>{
            dispatch(setCartData(response?.data))
        }).catch((err=>{
            console.log(err)
        }))
    }
}

export function updateCartItem (formData) {
    return async (dispatch) => {
        return appService.post(`/cart/update/item`, formData).then((response)=>{
            if (response.status !== 200) {
                throw Error(response);
            }
            return response.data;
        }).then((response)=>{
            dispatch(setCartData(response?.data))
            Toast.show({
                type: 'success',
                text1: formData?._id ? 'Item updated successfully' : 'Item added successfully'
            });
            Vibration.vibrate(1000, false);
        }).catch((err=>{
            Toast.show({
                type: 'error',
                text1: 'Failed to delete item'
            })
            console.log(err)
        }))
    }
}

export function deleteCartItem (formData) {
    return async (dispatch) => {
        return appService.post(`/cart/delete/item`, formData).then((response)=>{
            if (response.status !== 200) {
                throw Error(response);
            }
            return response.data;
        }).then((response)=>{
            dispatch(setCartData(response?.data))
            Toast.show({
                type: 'success',
                text1: 'Item deleted successfully'
            })
            Vibration.vibrate(1000, false);
        }).catch((err=>{
            Toast.show({
                type: 'error',
                text1: 'Failed to delete item'
            })
            console.log(err)
        }))
    }
}
