import Toast from 'react-native-toast-message';

import appService from "../../service/appService";

export function setDeliveryAddress(data){
    return{
        type: "SET_DELIVERY_ADDRESS",
        payload:data
    }
}

export function setDeliverylots(data){
    return{
        type: "SET_DELIVERY_SLOTS",
        payload:data
    }
}

export function setSelectedDeliverySlot(data){
    return{
        type: "SET_SELECTED_DELIVERY_SLOT",
        payload:data
    }
}



export function getDeliverySlots() {
    return async (dispatch) => {
        return appService.get(`/order/delivery/slot/list`).then((response)=>{
            if (response.status !== 200) {
                throw Error(response);
            }
            return response.data;
        }).then((response)=>{
            dispatch(setDeliverylots(response.data))
            if(response?.data?.length > 0){
                dispatch(setSelectedDeliverySlot(response?.data?.[0]))
            }
        }).catch((err=>{
            console.log(err)
            dispatch(setDeliverylots())
        }))
    }
}

export function createOrder (formData) {
    return appService.post(`/order/add`, formData).then((response)=>{
        if (response.status !== 200) {
            throw Error(response);
        }
        return response.data;
    }).then((response)=>{
        return response
    }).catch((err=>{
        console.log(err)
    }))
}