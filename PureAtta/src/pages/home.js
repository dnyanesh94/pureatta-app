import AsyncStorage from "@react-native-async-storage/async-storage";
import React,{useState, useEffect} from "react";
import { SafeAreaView, StyleSheet, View, StatusBar, Text, Image } from "react-native";
import Images from "../../assets/images/Images";
import PrimaryButton from "../components/primaryButton";
import appStyle from "../style/appStyle";
import { fSize, getHeight } from "../utils/responsive";
import { ScreenNames } from "../utils/screens";
import { MEDIUM } from "../utils/typography";


const Home = (props) => {
   
    const checkIfLoggedIn = async () => {
        let token = await AsyncStorage.getItem('token')
        if (token) {
            props.navigation.navigate(ScreenNames.APP_HOME)
        }else{
            props.navigation.navigate(ScreenNames.PHONE_LOGIN)
        }
    }

    useEffect(() => {
        StatusBar.setHidden(true);
        checkIfLoggedIn()
    }, [])

    return (
        <SafeAreaView style={[appStyle.container, styles.container]}>
            <View style={[appStyle.content, styles.content]}>
                <View style={styles.contentSection1}>
                    <Image
                        source={Images.logo}
                    />
                </View>
                <View style={styles.contentSection2}>
                    <Text style={styles.contentSection2Text}>
                        {"Get Fresh Atta at \nYour Doorstep"}
                    </Text>
                </View>
                <View style={styles.contentSection3}>
                    <PrimaryButton
                        title={"Get Started"}
                        onPress={() => {
                            props.navigation.navigate(ScreenNames.PHONE_LOGIN)
                        }}
                    />
                </View>
            </View>
        </SafeAreaView>
    )
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: '#fff',
    },
    content: {
        justifyContent: 'space-between',
        height: getHeight(80),
    },
    contentSection1: {
        height: "30%",
        justifyContent: 'center',
        alignItems: 'center',
    },
    contentSection2: {
        height: "30%",
        justifyContent: 'center',
        alignItems: 'center',
    },
    contentSection2Text: {
        fontFamily: MEDIUM,
        fontSize: fSize(24),
        textAlign: 'center',
        lineHeight: 29,
        color: "#592B02"
    },
    contentSection3: {
        height: "40%",
        justifyContent: 'center',
        alignItems: 'center',
    },


})

export default Home;