import React,{useState, useRef, useEffect} from "react";
import {StyleSheet, View, Text, TextInput} from "react-native";
import OTPInputView from '@twotalltotems/react-native-otp-input'


import PrimaryButton from "./components/primeButton";
import { fSize, getHeight, getWidth } from "../../utils/responsive";
import { MEDIUM, REGULAR } from "../../utils/typography";
import DisableButton from "./components/disableButton";
import Header from "./components/header";
import { ScreenNames } from "../../utils/screens";
import { verify } from "../../api/authentication";
import AsyncStorage from "@react-native-async-storage/async-storage";


const Otp = (props) => {
    const {routeKey, userId} = props.route.params

    const [otp, setOtp] = useState('')
    const [isError, setIsError] = useState(false)
    const otpRef = useRef(null)
    
    const onSubmit = async() => {
        try {
            let resp = await verify({
                userId,
                otp
            })
            if(resp?.data?.token){
                await AsyncStorage.setItem('token', resp?.data?.token)
                let token = AsyncStorage.getItem('token')
                if(token){
                    props.navigation.navigate(ScreenNames.APP_HOME)
                }
            }
        } catch (error) {
            console.log(JSON.stringify(error))
            setIsError(true)
        }
        
    }

    useEffect(() => {
        otpRef.current.focusField(0);
    }, []);

    return(
        <View style={styles.container}>
            <View style={styles.header}>
                <Header 
                    onBack={()=>{
                        props.navigation.goBack()
                    }}
                    title={"OTP Verification"}
                />
            </View>
            <View style={styles.content}>
                <View style={styles.contentSection1}>
                    <Text style={styles.otpLable}>
                        {`Please enter 4 digit OTP sent on ${routeKey === 'email' ? 'email' : 'mobile number' }`}
                    </Text>
                    <View style={{width: "100%", height: "60%"}}>
                        <OTPInputView
                            ref={otpRef}
                            style={styles.otpInput}
                            pinCount={4}
                            code={otp}
                            onCodeChanged={code => {
                                if(isError){
                                    setIsError(false)
                                }
                                setOtp(code)
                            }}
                            keyboardAppearance={"default"}
                            keyboardType={"number-pad"}
                            codeInputFieldStyle={[
                                styles.underlineStyleBase,
                                otp?.length === 4 && styles.validInput,
                                isError && styles.inValidInput
                            ]}
                            codeInputHighlightStyle={styles.borderStyleHighLighted}
                        />
                        {
                            isError &&    
                            <Text style={styles.errorText}>{"Please enter valid OTP"}</Text>
                        }
                    </View>
                </View>
                <View style={styles.contentSection2}>
                    {
                        (otp?.length === 4) ?
                        <PrimaryButton
                            title={"Verify OTP"}
                            onPress={()=>{
                                onSubmit()
                            }}
                            disabled={false}
                        />:
                        <DisableButton
                            title={"Verify OTP"}
                            onPress={()=>{}}
                            disabled={true}
                        />
                    }
                    <Text style={styles.helpText}>
                        {"Didn’t get OTP ?"}
                    </Text>
                    <Text style={styles.helpLink}>
                        {"Resend"}
                    </Text>
                </View>
                <View style={styles.contentSection3}>
                    <Text style={styles.privacyText}>
                        {"Don’t have an email ? "}
                        <Text style={styles.privacyLink}>
                            {"Contact Us"}
                        </Text>
                    </Text>
                </View>
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    container:{
        width: getWidth(100),
        height: getHeight(100),
        backgroundColor: "#F2921D",
    },
    header:{
        height: getHeight(15),
    },
    content:{
        height: getHeight(85),
        borderTopRightRadius: 30,
        borderTopLeftRadius: 30,
        backgroundColor: "#fff",
        paddingHorizontal: 22,
        paddingVertical: 32,
    },
    contentSection1:{
        height: "35%",
        justifyContent: 'space-between',
        alignItems: 'center',
        paddingHorizontal: 20,
    },
    otpLable:{
        fontFamily: REGULAR,
        fontSize: fSize(12),
        lineHeight: 16,
        color: "#4F4F4F",
        textAlign: 'center',
     },
    otpInput: {
        width: '100%', 
        height: "60%",
    },
    underlineStyleBase: {
        width: 60,
        height: 50,
        borderWidth: 0,
        borderBottomWidth: 2,
        borderBottomColor: "#BDBDBD",
        fontFamily: MEDIUM,
        fontSize: fSize(34),
        lineHeight: 41,
        color: "#000",
    },
    borderStyleHighLighted: {
        width: 60,
        height: 50,
        borderWidth: 0,
        borderBottomWidth: 2,
        borderBottomColor: "#F2921D",
        fontFamily: MEDIUM,
        fontSize: fSize(34),
        lineHeight: 41,
        color: "#000",
        
    },
    contentSection2:{
        height: "70%",
        paddingHorizontal: 20
    },
    helpText:{
        fontFamily: REGULAR,
        fontSize: fSize(12),
        color: "#231100",
        textAlign: 'center',
        marginTop: 25
    },
    helpLink:{
        fontFamily: MEDIUM,
        fontSize: fSize(16),
        lineHeight: 20,
        textAlign: 'center',
        color: "#F2921D",
        marginTop: 10
    },
    validInput:{
        borderBottomColor: "#F2921D"
    },
    inValidInput: {
        borderBottomColor: "#E5320B"
    },
    errorText: {
        fontFamily: REGULAR,
        fontSize: fSize(12),
        lineHeight: 16,
        color: "#E5320B",
        textAlign: 'right',
    }
})

export default Otp;