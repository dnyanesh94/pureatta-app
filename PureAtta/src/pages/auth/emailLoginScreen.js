import React,{useState} from "react";
import {SafeAreaView, StyleSheet, View, StatusBar, Text, TextInput} from "react-native";
import PrimaryButton from "./components/primeButton";
import { fSize, getHeight, getWidth } from "../../utils/responsive";
import { MEDIUM, REGULAR } from "../../utils/typography";
import DisableButton from "./components/disableButton";
import Header from "./components/header";
import { ScreenNames } from "../../utils/screens";


const EmailLogin = (props) => {

    const [email, setEmail] = useState('')
    const [isError, setIsError] = useState(false)

    const checkIfValidEmail = () => {    
        return email.match(
            /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
        );
    }

    const onSubmit = () =>{
        if(!checkIfValidEmail() ||  email?.toLocaleLowerCase() !== 'abc@gmail.com' ){
            setIsError(true)
            return
        }
        props.navigation.navigate({name:ScreenNames.OTP, params: { routeKey: 'email'}})
    }

    return(
        <View style={styles.container}>
            <View style={styles.header}>
                <Header 
                    onBack={()=>{
                        props.navigation.goBack()
                    }}
                    title={"Email Verification"}
                />
            </View>
            <View style={styles.content}>
                <View style={styles.contentSection1}>
                    <Text style={styles.emailLable}>{"Email Address"}</Text>
                    <TextInput
                        style={[
                            styles.emailInput,
                            checkIfValidEmail() && !isError && styles.validInput,
                            isError && styles.inValidInput
                        ]}
                        keyboardType={"email-address"}
                        keyboardAppearance={"default"}
                        enablesReturnKeyAutomatically={true}
                        placeholder={"Eg.johndoe@gmail.com"}
                        placeholderTextColor={"rgba(0, 0, 0, 0.6)"}
                        onChangeText={(value)=>{
                            if(isError){
                                setIsError(false)
                            }
                            setEmail(value)
                        }}
                    />
                    {
                        isError && 
                        <Text style={styles.errorText}>{"Please enter valid email address"}</Text>
                    }
                </View>
                <View style={styles.contentSection2}>
                    {
                        checkIfValidEmail() ?
                        <PrimaryButton
                            title={"Send OTP"}
                            onPress={()=>{
                                onSubmit()
                            }}
                            disabled={false}
                        />:
                        <DisableButton
                            title={"Send OTP"}
                            onPress={()=>{}}
                            disabled={true}
                        />
                    }
                    {/* <Text style={styles.helpText}>
                        {"Have trouble signing in ?"}
                        <Text style={styles.helpLink}>
                            {" Try Email "}
                        </Text>
                    </Text> */}
                </View>
                <View style={styles.contentSection3}>
                    <Text style={styles.privacyText}>
                        {"Don’t have an email ? "}
                        <Text style={styles.privacyLink}>
                            {"Contact Us"}
                        </Text>
                    </Text>
                </View>
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    container:{
        width: getWidth(100),
        height: getHeight(100),
        backgroundColor: "#F2921D",
    },
    header:{
        height: getHeight(15),
    },
    content:{
        height: getHeight(85),
        borderTopRightRadius: 30,
        borderTopLeftRadius: 30,
        backgroundColor: "#fff",
        paddingHorizontal: 22,
        paddingVertical: 32,
    },
    contentSection1:{
        height: "40%",
    },
    emailLable:{
        fontFamily: MEDIUM,
        fontSize: fSize(14),
        lineHeight: 21,
        color: "#231100"
    },
    emailInput:{
        borderWidth: 1,
        borderRadius: 3.5,
        height: 50,
        borderColor: 'lightgray',
        marginVertical: 10,
        padding: 15,
    },
    contentSection2:{
        height: "40%",
    },
    helpText:{
        fontFamily: REGULAR,
        fontSize: fSize(12),
        color: "#231100",
        textAlign: 'center',
        marginVertical: 25
    },
    helpLink:{
        fontFamily: MEDIUM,
        fontSize: fSize(16),
        lineHeight: 20,
        color: "#F2921D",
    },
    contentSection3:{
        height: "20%",
        justifyContent: 'flex-end',
        alignItems: 'center',
        paddingBottom: 15
    },
    privacyText: {
        fontFamily: REGULAR,
        fontSize: fSize(12),
        lineHeight: 20,
        color: "#4F4F4F",
        textAlign: 'center',
    },
    privacyLink:{
        fontFamily: REGULAR,
        fontSize: fSize(12),
        color: "#348BFF",
    },
    validInput:{
        borderColor: "#F2921D"
    },
    inValidInput: {
        borderColor: "#E5320B"
    },
    errorText: {
        fontFamily: REGULAR,
        fontSize: fSize(12),
        lineHeight: 16,
        color: "#E5320B",
        textAlign: 'right',
    }
})

export default EmailLogin;