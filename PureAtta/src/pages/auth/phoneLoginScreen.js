import React,{useState} from "react";
import {SafeAreaView, StyleSheet, View, StatusBar, Text, TextInput} from "react-native";
import PrimaryButton from "./components/primeButton";
import { fSize, getHeight, getWidth } from "../../utils/responsive";
import { MEDIUM, REGULAR } from "../../utils/typography";
import DisableButton from "./components/disableButton";
import Header from "./components/header";
import { ScreenNames } from "../../utils/screens";
import { login } from "../../api/authentication";


const PhoneLogin = (props) => {

    const [phoneNumber, setPhoneNUmber] = useState('')
    const [isError, setIsError] = useState(false)

    const checkIfInputLengthValid = () => {
        return phoneNumber && phoneNumber?.length >= 10
    }

    const checkIfValidInput = () => {
        return phoneNumber?.length >= 10
    }

    const onSubmit = async() =>{
        if(phoneNumber?.length > 10  || phoneNumber?.length < 10 ){
            setIsError(true)
            return
        }
        try {
            let resp = await login({phone:phoneNumber})
            props.navigation.navigate({
                name:ScreenNames.OTP, 
                params: { 
                    routeKey: 'phone', 
                    userId: resp?.data?.userId
                }
            })
        } catch (error) {
            console.log(JSON.stringify(error))
        }
    }

    return(
        <View style={styles.container}>
            <View style={styles.header}>
                <Header 
                    onBack={()=>{
                        props.navigation.goBack()
                    }}
                    title={"Welcome"}
                    subTitle={"Create account to get started"}
                />
            </View>
            <View style={styles.content}>
                <View style={styles.contentSection1}>
                    <Text style={styles.phoneLable}>{"Mobile Number (10-Digit)"}</Text>
                    <TextInput
                        style={[
                            styles.phoneInput,
                            checkIfValidInput() && !isError && styles.validInput,
                            isError && styles.inValidInput
                        ]}
                        keyboardType={"phone-pad"}
                        keyboardAppearance={"default"}
                        enablesReturnKeyAutomatically={true}
                        placeholder={"Eg.1234567890"}
                        placeholderTextColor={"rgba(0, 0, 0, 0.6)"}
                        maxLength={10}
                        onChangeText={(value)=>{
                            if(isError){
                                setIsError(false)
                            }
                            setPhoneNUmber(value)
                        }}
                    />
                    {
                        isError && 
                        <Text style={styles.errorText}>{"Please enter valid mobile number"}</Text>
                    }
                </View>
                <View style={styles.contentSection2}>
                    {
                        checkIfInputLengthValid() ?
                        <PrimaryButton
                            title={"Send OTP"}
                            onPress={()=>{ onSubmit()}}
                            disabled={true}
                        />:
                        <DisableButton
                            title={"Send OTP"}
                            onPress={()=>{}}
                            disabled={true}
                        />
                    }
                    <Text style={styles.helpText}>
                        {"Have trouble signing in ?"}
                        <Text style={styles.helpLink} onPress={()=>{props.navigation.navigate(ScreenNames.EMAIL_LOGIN)}}>
                            {" Try Email "}
                        </Text>
                    </Text>
                </View>
                <View style={styles.contentSection3}>
                    <Text style={styles.privacyText}>
                        {"By Signing up and Loging in you agree to our"}
                        <Text style={styles.privacyLink}>
                            {" Terms and Conditions"}
                        </Text> and 
                        <Text style={styles.privacyLink}>{" Privacy policy"}</Text>
                    </Text>
                </View>
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    container:{
        width: getWidth(100),
        height: getHeight(100),
        backgroundColor: "#F2921D",
    },
    header:{
        height: getHeight(15),
    },
    content:{
        height: getHeight(85),
        borderTopRightRadius: 30,
        borderTopLeftRadius: 30,
        backgroundColor: "#fff",
        paddingHorizontal: 22,
        paddingVertical: 32,
    },
    contentSection1:{
        height: "40%",
    },
    phoneLable:{
        fontFamily: MEDIUM,
        fontSize: fSize(14),
        lineHeight: 21,
        color: "#231100"
    },
    phoneInput:{
        borderWidth: 1,
        borderRadius: 3.5,
        height: 50,
        borderColor: 'lightgray',
        marginVertical: 10,
        padding: 15,
    },
    contentSection2:{
        height: "40%",
    },
    helpText:{
        fontFamily: REGULAR,
        fontSize: fSize(12),
        color: "#231100",
        textAlign: 'center',
        marginVertical: 25
    },
    helpLink:{
        fontFamily: MEDIUM,
        fontSize: fSize(16),
        lineHeight: 20,
        color: "#F2921D",
    },
    contentSection3:{
        height: "20%",
        justifyContent: 'flex-end',
        alignItems: 'center',
        paddingBottom: 15
    },
    privacyText: {
        fontFamily: REGULAR,
        fontSize: fSize(12),
        lineHeight: 20,
        color: "#4F4F4F",
        textAlign: 'center',
    },
    privacyLink:{
        fontFamily: REGULAR,
        fontSize: fSize(12),
        color: "#348BFF",
    },
    validInput:{
        borderColor: "#F2921D"
    },
    inValidInput: {
        borderColor: "#E5320B"
    },
    errorText: {
        fontFamily: REGULAR,
        fontSize: fSize(12),
        lineHeight: 16,
        color: "#E5320B",
        textAlign: 'right',
        // marginTop: 5
    }
})

export default PhoneLogin;