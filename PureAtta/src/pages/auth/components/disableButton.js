import React from 'react';
import { TouchableOpacity, StyleSheet, Text } from 'react-native';
import { fSize } from '../../../utils/responsive';
import { MEDIUM } from '../../../utils/typography';

const DisableButton = (props) => {
    const { title, onPress, disabled} = props;
    return(
        <TouchableOpacity style={styles.button} onPress={onPress} disabled={disabled}>
            <Text style={styles.buttonText}>
                {title}
            </Text>
        </TouchableOpacity>
    )
}

const styles = StyleSheet.create({
    button:{
        width: "100%",
        backgroundColor: "rgba(31, 31, 31, 0.12)",
        borderRadius: 100,
        justifyContent: 'center',
        alignItems: 'center',
        height: 48
    },
    buttonText:{
        fontFamily: MEDIUM,
        fontSize: fSize(14),
        lineHeight: 17,
        color: "#828282",
        textAlign: 'center'
    }
})

export default DisableButton