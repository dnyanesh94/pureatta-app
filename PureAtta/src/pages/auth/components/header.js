import React from 'react';
import {StyleSheet, View, Text, Image, TouchableOpacity} from 'react-native';
import { SafeAreaView } from 'react-native-safe-area-context';
import Images from '../../../../assets/images/Images';
import { fSize } from '../../../utils/responsive';
import { REGULAR, SEMI_BOLD } from '../../../utils/typography';

const Header = (props) => {
    const { onBack, title, subTitle } = props;

    return(
        <View style={styles.container}>
            <View style={styles.containerLeft}>
                <TouchableOpacity style={styles.backButton} onPress={()=>{onBack()}}>
                    <Image 
                        source={Images.arrowLeft}
                    />
                </TouchableOpacity>
            </View>
            <View style={styles.containerRight}>
                {
                    title &&
                    <Text style={styles.titleText}>
                        {title}
                    </Text>
                }
                {
                    subTitle &&
                    <Text style={styles.subTitleText}>
                        {subTitle}
                    </Text>
                }
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    container:{
        flexDirection:'row',
        height: '100%',
        paddingHorizontal: 10,
        paddingTop: 50
    },
    containerLeft:{
        width:"10%",
        height: 100,
    },
    backButton:{
        height: "100%",
        width: "100%"
    },
    containerRight:{
        width:"90%",
        height: 100,
    },
    titleText:{
        fontFamily: SEMI_BOLD,
        fontSize: fSize(20),
        lineHeight: 24,
        color: "#fff"
    },
    subTitleText:{
        fontFamily: REGULAR,
        fontSize: fSize(16),
        lineHeight: 20,
        color: "#fff"
    }

})

export default Header;