import React, { useEffect, useState } from "react";
import {StyleSheet, View, ScrollView, Image, Text, 
    TouchableOpacity, TextInput} from "react-native";
import { useDispatch, useSelector } from "react-redux";

import Images from "../../../../assets/images/Images";
import { fSize, getHeight, getWidth } from "../../../utils/responsive";
import { MEDIUM, REGULAR } from "../../../utils/typography";
import Header from "../components/header";
import AddressListModal from "../addressComponents/addressListModal";
import ButtonPrimary from "../addressComponents/buttonPrimary";
import ButtonSecondary from "../addressComponents/buttonSecondary";
import CartItem from "./components/cartItem";
import DeliverySlotDropdown from "./components/deliverySlotDropdown";
import {backNavigation, navigate} from '../../../root/navigation/index'
import { deleteCartItem } from "../../../redux/actions/cartAction";
import { getUserAddressList } from "../../../redux/actions/userAction";
import { createOrder, getDeliverySlots, setDeliveryAddress } from "../../../redux/actions/cartSummaryAction";
import { ScreenNames } from "../../../utils/screens";

const CartSummary = (props) => {
    const { data, } = useSelector(state=>state.cart)
    const { addressList } = useSelector(state=>state?.user)
    const { deliveryAddress , deliverySlots, selectedDeliverSlot} = useSelector(state=>state?.cartSummary)

    const dispatch  = useDispatch()

    const [addressListModal, setAddressListModal] = useState(false)
    const [comment, setComment] = useState('')

    const headerLeft = () => (
        <TouchableOpacity style={styles.iconContainer} onPress={()=> backNavigation()}>
            <Image source={Images.arrowLeftBlackIcon} style={styles.icon}/>
        </TouchableOpacity>
    )

    const headerMiddle = () => (
        <Text style={styles.headerMiddleText}>{"Cart Summary"}</Text>
    )
        
    const deleteItem = async(item) => {
        let formData = {
            _id: item?._id,
            cartId: data?._id
        }
        dispatch(deleteCartItem(formData))
    }

    useEffect(()=>{
        dispatch(getUserAddressList())
        dispatch(getDeliverySlots())
    },[])

    const onPressItem = (address) => {
        dispatch(setDeliveryAddress(address))
    }

    const extractAddress = () => {
        return (
            deliveryAddress &&
                deliveryAddress?.address1 + "," + 
                deliveryAddress?.address2 + "," + 
                deliveryAddress?.city)
    }

    const checkIfValidData = () => {
        return !data || data?.items?.length === 0 || !selectedDeliverSlot || !deliveryAddress
    }

    const onSubmit = async() =>{
        let items = []
        data?.items?.map(item=>{
            items.push({
                productId: item?.product?._id,
                variantId: item?.variant?._id,
                quantity: item?.quantity
            })
        })
        if(!checkIfValidData()){
            let formData = {
                addressId: deliveryAddress?._id, 
                slotId: selectedDeliverSlot?._id,  
                comment: comment, 
                items:items
            }
            try {
                let resp = await createOrder(formData);
                if(resp){
                    navigate(ScreenNames.ORDER_SUCCESS)
                }

            } catch (error) {
                console.log(error)
            }
        }
    }

    return(
        <View style={styles.container}>
            <View style={styles.header}>
                <Header 
                    headerLeft={headerLeft}
                    headerMiddle={headerMiddle}
                    headerRight={null}
                />
            </View>
            <ScrollView  contentContainerStyle={styles.content}>
                <View style={{paddingBottom: 50 }}>
                    <View style={styles.addressBar}>
                        <View style={styles.addressBarLeft}>
                            <Image source={Images.locationIcon} />
                        </View>
                        <View style={styles.addressBarMiddle}>
                            <Text  style={styles.addressBarInput}>{extractAddress() || 'Add Address to Proceed'}</Text>
                        </View>
                        <View style={styles.addressBarRight}>
                            <TouchableOpacity onPress={()=>{setAddressListModal(true)}}>
                                <Image source={Images.addIcon} />
                            </TouchableOpacity>
                        </View>
                    </View>
                    {
                        data?.items?.map(item=>(
                            <CartItem item={item} key={item?._id} deleteItem={deleteItem}/>
                        ))
                    }
                    <View style={styles.timeSlotContainer}>
                        <Text style={styles.timeSlotText}>{"Select Delivery Time Slot"}</Text>
                        <DeliverySlotDropdown 
                            data={deliverySlots || []}
                            label={"Select Delivery Time"}
                            selectedDeliverSlot={selectedDeliverSlot}
                        />
                    </View>
                    <View style={styles.instructionContainer}>
                        <Text style={styles.instructionText}>{"Add Instructions"}</Text>
                        <TextInput 
                            placeholder={"Eg. Leave order at security guard"} 
                            placeholderTextColor={"#828282"}
                            style={styles.instructionInput}
                            multiline={true}
                            numberOfLines={15}
                            textAlignVertical={"top"}
                            onChangeText={(text)=>setComment(text)}
                        />
                    </View>    
                    <View style={styles.billContainer}>
                        <Text style={styles.billText}>{"Bill Details"}</Text>
                        <View style={styles.billDetails}>
                            <View style={styles.billDetailsSection}>
                                <Text style={styles.lableText}>{"Item Total"}</Text>
                                <Text style={styles.lableValue}>Rs. {data?.total}</Text>
                            </View>
                            <View style={[styles.billDetailsSection, {borderBottomWidth: 1, borderBottomColor: "#000"}]}>
                                <Text style={styles.lableText}>{"Delivery Charges"}</Text>
                                <Text style={[styles.lableValue, {color:"#FF8A1E"}]}>Free</Text>
                            </View>
                            <View style={styles.billDetailsSection}>
                                <Text style={styles.lableText}>{"Total Amount"}</Text>
                                <Text style={styles.lableValue}>Rs. {data?.total}</Text>
                            </View>
                        </View>
                    </View>    
                    <View style={styles.noteContainer}>
                        <Text style={styles.noteText}>
                            Note: We are accepting <Text style={{color:"#FF8A1E"}}>COD only.</Text>{"\n"}
                            Review your order & address details before placing order.
                        </Text>
                    </View>
                    {
                        checkIfValidData() ? 
                        <View style={styles.buttonContainer}>
                            <ButtonSecondary 
                                title={"Place Order"}
                                onPress={()=>{props.navigation.goBack()}}
                            />
                        </View>
                        :
                        <View style={styles.buttonContainer}>
                            <ButtonPrimary 
                                title={"Place Order"}
                                onPress={()=>{onSubmit()}}
                            />
                        </View>
                    }
                </View>
            </ScrollView>
            {
                addressListModal &&
                <AddressListModal 
                    onClose={()=>{setAddressListModal(false)}}
                    addressList={addressList}
                    onPress={onPressItem}
                    deliveryAddress={deliveryAddress}
                />
            }
        </View>
    )
}

const styles = StyleSheet.create({
    container:{
        width: getWidth(100),
        height: getHeight(100),
        backgroundColor: "#F7F7F7",
    },
    header:{
        height: getHeight(10),
    },
    headerMiddleText:{
        fontFamily: REGULAR,
        fontSize: fSize(20),
        lineHeight: 28,
        color: "#1C1B1F",
        textAlign: 'left'
    },
    iconContainer:{
        padding: 10,
        justifyContent: 'center',
        alignItems: 'flex-start'
    },
    icon:{
    
    },
    content:{
        paddingTop: 10,
        paddingHorizontal: 20,
    },
    addressBar:{
        flexDirection: 'row',
        backgroundColor: "#FFFFFF",
        paddingHorizontal:10,
        paddingVertical: 0,
        justifyContent: 'center',
        height: 50,
        borderRadius: 8,
        marginBottom: 5
    },
    addressBarLeft:{
        flex:0.1,
        justifyContent: 'center',
        height: "100%"
    },
    addressBarMiddle:{
        flex:0.8,
        justifyContent: 'center',
        height: "100%"
    },
    addressBarInput:{
        fontFamily: MEDIUM,
        fontSize: fSize(14),
        lineHeight: 17,
        color: "#828282",
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: 5
    },
    addressBarRight:{
        flex:0.1,
        justifyContent: 'center',
        height: "100%"
    },
    timeSlotContainer:{
        marginTop: 20,
        marginBottom: 10
    },
    timeSlotText:{
        fontFamily: MEDIUM,
        fontSize: fSize(14),
        lineHeight: 16,
        color: "#231100",
        marginBottom: 10
    },
    instructionContainer:{
        marginTop: 20,
        marginBottom: 20,
    },
    instructionText:{
        fontFamily: MEDIUM,
        fontSize: fSize(14),
        lineHeight: 16,
        color: "#231100",
        marginBottom: 10
    },
    instructionInput:{
        fontFamily: REGULAR,
        fontSize: fSize(14),
        lineHeight: 17,
        color: "#828282",
        borderWidth: 0.5,
        borderColor: "#BDBDBD",
        backgroundColor: "#fff",
        height: 100,
        borderRadius: 8,
        paddingTop: 8,
        padding: 8
    },
    billContainer:{
        marginTop: 10,
    },
    billText:{
        fontFamily: MEDIUM,
        fontSize: fSize(14),
        lineHeight: 16,
        color: "#231100",
        marginBottom: 10
    },
    billDetails:{
        paddingVertical:15,
        paddingHorizontal: 10,
        backgroundColor: '#FFFFFF',
        borderRadius: 8
    },
    billDetailsSection:{
        flexDirection: 'row', 
        paddingVertical: 10,
        justifyContent: 'space-between',
    },
    lableText:{
        fontFamily: REGULAR,
        fontSize: fSize(14),
        lineHeight: 17,
        color: "#231100",
    },
    lableValue:{
        fontFamily: MEDIUM,
        fontSize: fSize(14),
        lineHeight: 16,
        color: "#231100",
    },
    noteContainer:{
        marginTop: 15
    },
    noteText:{
        fontFamily: REGULAR,
        fontSize: fSize(12),
        lineHeight: 18,
        color: "#828282"
    },
    buttonContainer:{
        width: "100%",
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: 20,
    }
})

export default CartSummary;