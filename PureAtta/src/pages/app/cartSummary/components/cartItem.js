import React from 'react';
import { StyleSheet, View, Image, Text, TouchableOpacity } from 'react-native';
import Images from '../../../../../assets/images/Images';
import { fSize } from '../../../../utils/responsive';
import { MEDIUM, REGULAR } from '../../../../utils/typography';

const CartItem = ({item, deleteItem}) => {
    return (
        <View style={[styles.itemContainer, { zIndex: 100}]}>
            <View style={styles.itemDetails}>
                <View style={styles.productNameContainer}>
                    <Text style={styles.productName}>{item?.product?.name}</Text>
                    <TouchableOpacity onPress={()=>{deleteItem(item)}}>
                        <Image source={Images.deleteIcon} />
                    </TouchableOpacity>
                </View>
                <View style={styles.infoContainer}>
                    <View style={styles.priceContainer}>
                        <Text style={styles.priceText}>Rs.{item?.variant?.price}</Text>
                    </View>
                    <View style={styles.quantityContainer}>
                        <Text style={styles.quantityLabel}>{"quantity : "}</Text>
                        <Text style={styles.quantityValue}>{item?.quantity}</Text>
                    </View>
                </View>
                <View style={[styles.extraContainer]}>
                    <Text style={styles.extraRightTextValueText}>{item?.variant?.name}</Text>
                </View>
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    itemContainer: {
        width: "100%",
        backgroundColor: '#ffffff',
        marginVertical: 5,
        padding: 10,
        paddingVertical:15,
        borderRadius: 8,
        shadowColor: '#000',
        shadowOffset: { width: 0, height: 0.2 },
        shadowOpacity: 0.1,
        shadowRadius: 3,
        elevation: 5,
        overflow: 'visible',
    },
    itemDetails: {},
    itemRight: {
        flexDirection: 'row',
    },
    productNameContainer: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        borderBottomWidth: 1,
        borderBottomColor: 'whitesmoke',
        paddingBottom: 10
    },
    productName: {
        fontFamily: REGULAR,
        fontSize: fSize(14),
        lineHeight: 17,
        color: "#231100",
    },
    infoContainer:{
        flexDirection: 'row', 
        justifyContent: 'space-between', 
        marginTop: 10
    },
    priceContainer: {
        flexDirection: 'row',
    },
    priceText: {
        fontFamily: MEDIUM,
        fontSize: fSize(16),
        lineHeight: 20,
        color: "#231100",
    },
    quantityContainer: {
        flexDirection: 'row',
    },
    quantityLabel: {
        fontFamily: REGULAR,
        fontSize: fSize(16),
        lineHeight: 20,
        color: "#231100",
    },
    quantityValue: {
        fontFamily: MEDIUM,
        fontSize: fSize(16),
        lineHeight: 20,
        color: "#231100",
    },
    extraContainer: {
        marginTop: 10,
        flexDirection: 'row',
        justifyContent: 'space-between'
    },
    extraRightTextValueText:{
        textAlign: 'center',
        fontFamily: REGULAR,
        fontSize: fSize(14),
        lineHeight: 17,
        color: "#231100",
    }

})

export default CartItem;