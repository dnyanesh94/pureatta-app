import React from "react";
import {StyleSheet, View, Text,  Image,
    TouchableOpacity} from "react-native";
import Images from "../../../../assets/images/Images";

import { navigate } from "../../../root/navigation";
import { fSize, getHeight, getWidth } from "../../../utils/responsive";
import { ScreenNames } from "../../../utils/screens";
import { MEDIUM, REGULAR, SEMI_BOLD } from "../../../utils/typography";

const OrderSuccess = (props) => {
    return(
        <View style={styles.container}>
            <View style={styles.successContainer}>
                <View style={styles.imageContainer}>
                    <Image 
                        source={Images.orderSuccess}
                    />
                </View>
                <View style={styles.messageTextContainer}>
                    <Text style={styles.messageText}>
                        {"Order Placed Successfully!"}
                    </Text>
                    <Text style={styles.messageSubText}>
                        {"You can view order details in My Orders."}
                    </Text>
                </View>
                <View style={styles.buttonContainer}>
                    <TouchableOpacity style={styles.returnButton}
                        onPress={()=>{navigate(ScreenNames.APP_HOME)}}
                    >
                        <Text style={styles.returnButtonText}>
                            {"Back to Home"}
                        </Text>
                    </TouchableOpacity>
                </View>
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    container:{
        width: getWidth(100),
        height: getHeight(100),
        backgroundColor: "#F7F7F7",
    },
    successContainer:{
        height: getHeight(100),
        justifyContent: 'center',
        // paddingHorizontal: getWidth(10)
    },
    imageContainer:{
        justifyContent: 'center',
        alignItems: 'center',
        marginBottom: 10
    },
    messageTextContainer:{
        marginVertical: 20,
        justifyContent: 'center',
        alignItems: 'center'
    },
    messageText:{
        fontFamily: SEMI_BOLD,
        fontSize: fSize(16),
        lineHeight: 20,
        color: "#231100",
    },
    messageSubText:{
        marginTop: 10,
        fontFamily: REGULAR,
        fontSize: fSize(14),
        lineHeight: 20,
        color: "#828282",
    },
    buttonContainer:{
        justifyContent: 'center',
        alignItems: 'center'
    },
    returnButton:{
        width: "90%",
        height: 48,
        backgroundColor :"rgba(242, 146, 29, 0.1)",
        borderRadius: 100,
        justifyContent: 'center',
        alignItems: 'center',
        borderWidth: 1,
        borderColor: "#F2921D",
        marginTop: 20
    },
    returnButtonText:{
        fontFamily: MEDIUM,
        fontSize: fSize(16),
        lineHeight: 20,
        color: "#F2921D",
    },
})

export default OrderSuccess;