import React, {useState, useEffect} from 'react';
import { StyleSheet, TouchableOpacity, View, Image, Text, ScrollView, Alert } from 'react-native';
import Swiper from 'react-native-swiper';

import Images from '../../../../assets/images/Images';
import { fSize, getHeight, getWidth } from '../../../utils/responsive';
import { BOLD, MEDIUM, REGULAR, SEMI_BOLD } from '../../../utils/typography';
import CustomDropdown from './components/customDropdown';
import Header from '../components/header';
import { productPageData, validateVariant } from '../../../api/productDetailsPageApi';
import { filepathExtractor } from '../../../utils/images';
import { backNavigation, navigate } from '../../../root/navigation';
import HeaderCart from '../components/headerCart';
import { useDispatch } from 'react-redux';
import { updateCartItem } from '../../../redux/actions/cartAction';

const ProductDetails = (props) => {
    const {productId} = props.route.params
    const dispatch  = useDispatch()
    const [product, setProduct]  = useState()
    const [selectedVariant, setSelectedVariant] = useState()
    const [updatekey, setUpdateKey] = useState(0)

    const headerLeft = () => (
        <TouchableOpacity style={styles.iconContainer} onPress={()=>backNavigation()}>
            <Image source={Images.arrowLeftBlackIcon} style={styles.icon}/>
        </TouchableOpacity>
    )

    const headerMiddle = () => (
        <Text style={styles.headerMiddleText}>{product?.name}</Text>
    )

    const headerRight = () => (
        <HeaderCart update={updatekey}/>
    )

    const getPageData = async() => {
        try {
            let resp = await productPageData(productId)
            setProduct(resp?.data)
        } catch (error) {
            console.log(JSON.stringify(error?.response?.data))
        }
    }

    useEffect(()=>{
        if(productId){
            getPageData()
        }
    },[productId])

    useEffect(()=>{
        if(product){
            setSelectedVariant(product?.variants?.[0])
        }
    },[product])

    const chekIfVariantExists = () => {
        return selectedVariant && Object.keys(selectedVariant)?.length > 0 && selectedVariant?._id
    }


    const onChangeVariantValue = async(item) => {
        let tmpObj = {...selectedVariant.attributes, ...item}
        try {
            let resp = await validateVariant({
                productId: product?._id,
                attributes: tmpObj
            })
            if(resp?.data){
                setSelectedVariant(resp?.data)
            }
        } catch (error) {
            console.log(error)
            setSelectedVariant({attributes:tmpObj}) 
        }
    }

    const images = selectedVariant?.images?.length > 0 ? selectedVariant?.images : product?.images
    
    const  addToCart  = async () => {
        let formData = {
            variantId: selectedVariant?._id,
            productId: productId,
            quantity: 1
        }
        dispatch(updateCartItem(formData))
    }

    return(
        <View style={styles.container}>
            <View style={styles.header}>
                <Header 
                    headerLeft={headerLeft}
                    headerMiddle={headerMiddle}
                    headerRight={headerRight}
                />
            </View>
            <View style={styles.content}>
                <View style={styles.bannerContainer}>
                    {
                        product?.images?.length > 0 &&
                        <Swiper 
                            style={styles.slideWrapper} 
                            showsButtons={false}
                            autoplay={true}
                            activeDotColor={"#FFF"}
                            dotColor={"rgba(255, 255, 255, 0.4)"}
                            dotStyle={{
                                width: 10,
                                height: 10
                            }}
                            activeDotStyle={{
                                width: 10,
                                height: 10
                            }}
                        >
                        {
                            images?.map((item, index)=>{
                                return(
                                    <TouchableOpacity key={item._id} style={styles.bannerSlide}>
                                        <Image 
                                            style={{width: "100%", height: "100%"}}
                                            source={{uri:filepathExtractor(item?.url)}} 
                                        />
                                    </TouchableOpacity>
                                )
                            })
                        }
                        </Swiper>
                    }
                </View>
                <ScrollView style={styles.informationContainer}>
                    <View style={styles.productHeaderContainer}>
                        <View style={styles.nameContainer}>
                            <Text style={styles.nameText}>{product?.name}</Text>
                        </View>
                        <View style={styles.ratingContainer}>
                            <Text style={styles.ratingText}>
                                (
                                    <Image source={Images.starIcon} /> 
                                    {product?.rating} 
                                )
                            </Text>
                        </View>
                        <View style={styles.priceContainer}>
                            <Text style={styles.priceText}>{chekIfVariantExists() && `Rs. ${selectedVariant?.price}`}</Text>
                        </View>
                    </View>
                    <View style={styles.productAttributeContainer}>
                        <View style={styles.productAttribute}>
                            <Image source={Images.leafIcon} />
                            <Text style={styles.attributeText}>
                                {"Fresh"}
                            </Text>
                        </View>
                        <View style={styles.productAttribute}>
                            <Image source={Images.heartIcon} />
                            <Text style={styles.attributeText}>
                                {"Healthy"}
                            </Text> 
                        </View>
                        <View style={styles.productAttribute}>
                            <Image source={Images.dropIcon} />
                            <Text style={styles.attributeText}>
                                {"Pure"}
                            </Text>
                        </View>
                    </View>
                    <View style={styles.productDescriptionContainer}>
                        <Text style={styles.descriptionText}>- {product?.description}</Text>
                        <Text style={styles.descriptionText}>- {product?.long_description}</Text>
                    </View>
                    {
                        product?.attributes?.map((attribute, attributeIndex)=>(
                            <View style={styles.productVariants} key={attributeIndex}>
                                <View style={styles.variantLeft}>
                                    <Text style={styles.variantText}>{attribute?.name}</Text>
                                </View>
                                <View style={styles.variantRight}>
                                    <CustomDropdown 
                                        selectedVariant={selectedVariant} 
                                        label={`Select ${attribute?.name}`} 
                                        data={attribute} 
                                        onSelect={onChangeVariantValue} 
                                    />
                                </View>
                            </View>
                        ))
                    }
                    {
                        chekIfVariantExists() &&
                        <View style={styles.productInformation}>
                            <Text style={[styles.informationText, {fontFamily: MEDIUM}]}>{selectedVariant?.name}</Text>
                        </View>
                    }
                    <View style={styles.productInformation}>
                        <Text style={styles.informationText}>{"Expirary Date: 6 months from grinding"}</Text>
                    </View>
                    {
                        chekIfVariantExists() ?
                        <View style={styles.buttonContainer}>
                            <TouchableOpacity style={styles.button} onPress={()=>{addToCart()}}>
                                <Text style={styles.buttonText}>{"Add to Cart"}</Text>
                            </TouchableOpacity>
                        </View>:
                        <View style={styles.buttonContainer}>
                            <Text style={styles.outofStockText}>{"Currently unavailable"}</Text>
                        </View>
                    
                    }
                </ScrollView>
            </View>
        </View>    
    )
}

const styles = StyleSheet.create({
    container:{
        width: getWidth(100),
        height: getHeight(100),
        backgroundColor: "#E5E5E5",
    },
    header:{
        height: getHeight(10),
    },
    headerMiddleText:{
        fontFamily: REGULAR,
        fontSize: fSize(20),
        lineHeight: 28,
        color: "#1C1B1F",
        textAlign: 'left'
    },
    iconContainer:{
        padding: 10,
        justifyContent: 'center',
        alignItems: 'center'
    },
    icon:{
    
    },
    content:{
        height: getHeight(85),
    },
    bannerContainer:{
        height: "35%",
    },
    slideWrapper: {
        justifyContent: 'flex-start',
    },
    bannerSlide: {
        width: "100%",
    },
    informationContainer:{
        paddingHorizontal: 20,
        paddingVertical: 30,
        backgroundColor: "#F7F7F7",
        height: "100%",
        borderTopLeftRadius: 24,
        borderTopRightRadius: 24,
        marginTop: -20
    },
    productHeaderContainer:{
       flexDirection: 'row' 
    },
    nameContainer:{
        flex: 0.5,
        justifyContent: 'flex-start',
        alignContent: 'center',
    },
    nameText:{
        fontFamily: SEMI_BOLD,
        fontSize: fSize(18),
        lineHeight: 22,
        color: "#231100",
    },
    ratingContainer:{
        flex: 0.2,
        // justifyContent: 'flex-end',
        alignContent: 'center',
        marginTop: 1,
        // backgroundColor: 'red'
    },
    ratingText:{
        fontFamily: REGULAR,
        fontSize: fSize(12),
        lineHeight: 16,
        color: "#4F4F4F",
        textAlign: 'right'
    },
    priceContainer:{
        flex: 0.3,
        justifyContent: 'flex-end',
        alignContent: 'flex-end'
    },
    priceText:{
        fontFamily: SEMI_BOLD,
        fontSize: fSize(18),
        lineHeight: 22,
        color: "#231100",
        textAlign: 'right'
    },
    productAttributeContainer:{
        marginVertical: 10,
        paddingVertical: 10,
        flexDirection: 'row',
        justifyContent: 'space-between'
    },
    productAttribute:{
        paddingVertical: 8,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: "rgba(242, 146, 29, 0.15)",
        borderRadius: 4,
        flex: 0.32,
        flexDirection: 'row'
    },
    attributeText:{
        fontFamily: REGULAR,
        fontSize: fSize(14),
        lineHeight: 17,
        color: "#231100",
        paddingLeft: 2
    },
    productDescriptionContainer:{
        marginVertical: 8
    },
    descriptionText:{
        fontFamily: REGULAR,
        fontSize: fSize(14),
        lineHeight: 17,
        color: "#4F4F4F",
    },
    productVariants:{
        marginTop: 2,
        paddingVertical: 4,
        flexDirection: 'row'
    },
    variantLeft:{
        flexDirection: 'row',
        flex: .3,
        alignItems: 'center'
    },
    variantText:{
        fontFamily: MEDIUM,
        fontSize: fSize(14),
        lineHeight: 16,
        color: "#231100",
    },
    variantRight:{
        flex: 0.5,
    },
    productInformation:{
        marginVertical: 5,
    },
    informationText:{
        fontFamily: REGULAR,
        fontSize: fSize(12),
        lineHeight: 16,
        color: "#4F4F4F",
    },
    buttonContainer:{
        marginVertical: 20,
        paddingVertical: 20,
        justifyContent: 'center',
    },
    button:{
        height: 48,
        backgroundColor :"#F2921D",
        borderRadius: 100,
        justifyContent: 'center',
        alignItems: 'center'
    },
    buttonText:{
        fontFamily: MEDIUM,
        fontSize: fSize(16),
        lineHeight: 20,
        color: "#FFFFFF",
    },
    outofStockText:{
        fontFamily: BOLD,
        fontSize: fSize(16),
        lineHeight: 20,
        color: "#F2921D",
        textAlign: 'center'
    }
})

export default ProductDetails;