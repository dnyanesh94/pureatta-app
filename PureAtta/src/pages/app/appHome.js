import React, { useState, useEffect } from "react";
import {StyleSheet, View, ScrollView, Image, Text, 
    TouchableOpacity, TextInput} from "react-native";
import Swiper from 'react-native-swiper'

import Images from "../../../assets/images/Images";
import { fSize, getHeight, getWidth } from "../../utils/responsive";
import { REGULAR, SEMI_BOLD } from "../../utils/typography";
import Header from "./components/header";
import Sidebar from "./components/Sidebar";
import ProductItem from "./components/productItem";
import CategoryItem from "./components/categoryItem";
import { homePageData } from "../../api/homePageApi";
import HeaderCart from "./components/headerCart";
import { useDispatch, useSelector } from "react-redux";
import { getHomeData } from "../../redux/actions/homeAction";
import { useIsFocused } from "@react-navigation/native";

const AppHome = (props) => {
    const dispatch = useDispatch()
    const isFocused = useIsFocused()
    const { data } = useSelector(state=>state.home)

    const [isSidebarOpen, setIsSidebarOpen] = useState(false);
    
    const headerLeft = () => (
        <TouchableOpacity style={styles.iconContainer} onPress={()=>{setIsSidebarOpen(true)}}>
            <Image source={Images.sideMenuIcon} style={styles.icon}/>
        </TouchableOpacity>
    )

    const headerMiddle = () => (
        <Text style={styles.headerMiddleText}>{"Pure Atta"}</Text>
    )

    const headerRight = () => (
       <HeaderCart />
    )


    useEffect(()=>{
        dispatch(getHomeData())
    },[isFocused])

    return(
        <View style={styles.container}>
            <View style={styles.header}>
                <Header 
                    headerLeft={headerLeft}
                    headerMiddle={headerMiddle}
                    headerRight={headerRight}
                />
            </View>
            <View style={styles.content}>
                <View style={styles.locationContainer}>
                    <Image source={Images.markerIcon}/>
                    <Text style={styles.locationText}>{"Megacenter, Hadapsar"}</Text>
                </View>
                <View style={styles.searchContainer}>
                    <TextInput 
                        style={styles.searchInput}
                        placeholder={"Search Atta"}
                        placeholderTextColor={"rgba(0, 0, 0, 0.6)"}
                    />
                    <TouchableOpacity style={styles.searchButton}>
                        <Image source={Images.searchIcon}/>
                    </TouchableOpacity>
                </View>
                <View style={styles.scrollContainer}>
                    <ScrollView>
                        <View style={styles.bannerContainer}>
                            <Swiper 
                                style={styles.slideWrapper} 
                                showsButtons={false}
                                autoplay={true}
                                activeDotColor={"#FF8A1E"}
                                dotColor={"rgba(255, 138, 30, 0.15)"}
                                dotStyle={{
                                    width: 10,
                                    height: 10
                                }}
                                activeDotStyle={{
                                    width: 10,
                                    height: 10
                                }}
                            >
                            {
                                ["1","2","3", "4"].map((item)=>(
                                    <TouchableOpacity key={item} style={styles.bannerSlide}>
                                        <Image 
                                            style={{width: "100%"}}
                                            source={Images.banner} 
                                        />
                                    </TouchableOpacity>
                                ))
                            }
                            </Swiper>
                        </View>
                        <View style={styles.categoryContainer}>
                            <View style={styles.categoryTitle}>
                                <Image  source={Images.categoryIcon} style={styles.categoryIcon} />
                                <Text style={styles.categoryTitleText}>
                                    {"Explore Atta Categories"}
                                </Text>
                            </View>  
                            <View style={{flexDirection: 'row'}}>
                            {
                                data?.categories?.map((item,index)=>(
                                    <CategoryItem item={item} key={index}/>
                                ))
                            }
                            </View>
                        </View>
                        <View style={styles.popularProduct}>
                            <View style={styles.popularProductTitle}>
                                <Image  source={Images.popularProductIcon} />
                                <Text style={styles.popularProductTitleText}>
                                    {"Most Ordered"}
                                </Text>
                            </View>
                            {
                                data?.products?.map((item,index)=>(
                                    <ProductItem item={item} key={index} />
                                ))
                            }
                        </View>
                    </ScrollView>
                </View>
            </View>
            {isSidebarOpen && 
                <Sidebar 
                    onClose={()=>{setIsSidebarOpen(false)}}
                    {...props}
                />
            }
        </View>
    )
}

const styles = StyleSheet.create({
    container:{
        width: getWidth(100),
        height: getHeight(100),
        backgroundColor: "#E5E5E5",
    },
    header:{
        height: getHeight(10),
    },
    headerMiddleText:{
        fontFamily: SEMI_BOLD,
        fontSize: fSize(22),
        lineHeight: 27,
        color: "#1C1B1F",
        textAlign: 'center'
    },
    icon:{
    
    },
    content:{
        height: getHeight(85),
    },
    locationContainer:{
        height: "6%",
        backgroundColor: "rgba(255, 138, 30, 0.15)",
        flexDirection: 'row',
        paddingHorizontal: 20,
        alignItems: 'center'
    },
    locationText:{
        fontFamily: REGULAR,
        fontSize: fSize(14),
        lineHeight: 17,
        color: "#000000",
        textAlign: 'center',
        paddingLeft: 5
    },
    searchContainer:{
        height: "10%",
        flexDirection: 'row',
        paddingHorizontal: 10,
        alignItems: 'center',
        justifyContent: 'space-between'
    },
    searchInput: {
        borderWidth: 1,
        borderColor: "rgba(0, 0, 0, 0.2)",
        height: 40,
        width: "80%",
        borderRadius: 4,
        fontFamily: REGULAR,
        fontSize: fSize(14),
        lineHeight: 17,
        paddingLeft: 12
    },
    searchButton:{
        height: 40,
        width: "15%",
        borderRadius: 4,
        backgroundColor: "#FF8A1E",
        justifyContent: 'center',
        alignItems: 'center'
    },
    scrollContainer:{
        height: "85%"
    },
    slideWrapper: {
        justifyContent: 'flex-start',
        height: 200,
    },
    bannerSlide: {
        width: "100%",
        height: 200,
    },
    categoryContainer:{
    
    },
    categoryTitle:{
        flexDirection: 'row',
        paddingLeft: 20,
        paddingBottom: 20
    },
    categoryIcon:{
        marginTop: -4
    },
    categoryTitleText:{
        fontFamily: SEMI_BOLD,
        fontSize: fSize(18),
        lineHeight: 22,
        color: "#231100",
        textAlign: 'center',
        paddingLeft: 10
    },
    popularProduct:{
        paddingTop: 40,
        padding:20
    },
    popularProductTitle:{
        flexDirection: 'row'
    },
    popularProductTitleText:{
        fontFamily: SEMI_BOLD,
        fontSize: fSize(18),
        lineHeight: 22,
        color: "#9D4A00",
        textAlign: 'center',
        paddingLeft: 10
    },
})

export default AppHome;