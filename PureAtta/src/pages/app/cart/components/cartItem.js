import React from 'react';
import { StyleSheet, View, TouchableOpacity, Image, Text } from 'react-native';
import NumericInput from 'react-native-numeric-input'

import Images from '../../../../../assets/images/Images';
import { fSize } from '../../../../utils/responsive';
import { MEDIUM, REGULAR } from '../../../../utils/typography';
import { filepathExtractor } from '../../../../utils/images'



const CartItem = ({item, updateItem, deleteItem}) => {
    const image = item?.variant?.image  ? item?.variant?.image : item?.product?.image
    return (
        <View style={styles.itemContainer}>
            <View style={styles.itemDetails}>
                <View style={styles.itemLeft}>
                    <Image source={{uri: filepathExtractor(image?.url)}} style={styles.itemLeftImage} />
                </View>
                <View style={styles.itemRight}>
                    <View style={styles.productNameContainer}>
                        <Text style={styles.productName}>{item?.product?.name}</Text>
                        <TouchableOpacity onPress={()=>{deleteItem(item)}} >
                            <Image source={Images.deleteIcon} />
                        </TouchableOpacity>
                    </View>
                    <View style={styles.priceContainer}>
                        <Text style={styles.priceText}>
                            Rs.{item?.variant?.price}
                        </Text>
                    </View>
                    <View style={[styles.variantContainer, { zIndex: 200 }]}>
                        <View style={styles.variantItem}>
                            <Text>{item?.variant?.name}</Text>
                        </View>
                    </View>
                    <View>
                    <NumericInput 
                        key={item?._id}
                        initValue={Number(item?.quantity)}
                        onChange={value =>{
                            if(value >= 1){
                                updateItem(item, value)
                            }
                        }} 
                        totalHeight={30} 
                        iconSize={25}
                        step={1}
                        minValue={1}
                        valueType='integer'
                        rounded 
                        textColor='#B0228C' 
                        iconStyle={{ color: 'white' }} 
                        rightButtonBackgroundColor='#F2921D' 
                        leftButtonBackgroundColor='#F2921D'
                    />
                    </View>
                </View>
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    itemContainer: {
        width: "100%",
        backgroundColor: '#ffffff',
        marginVertical: 10,
        padding: 20,
        borderRadius: 8,
        shadowColor: '#000',
        shadowOffset: { width: 0, height: 0.2 },
        shadowOpacity: 0.4,
        shadowRadius: 3,
        elevation: 5,
        overflow: 'visible',
    },
    itemDetails: {
        flexDirection: 'row',
    },
    itemLeft: {
        flex: 0.3,
    },
    itemLeftImage: {
        height: 100,
        aspectRatio: 1,
        borderRadius: 8
    },
    itemRight: {
        flex: 0.7,
        paddingLeft: 30
    },
    productNameContainer: {
        flexDirection: 'row',
        justifyContent: 'space-between',
    },
    productName: {
        fontFamily: REGULAR,
        fontSize: fSize(14),
        lineHeight: 17,
        color: "#231100",
    },
    priceContainer: {
        flexDirection: 'row',
        marginTop: 2
    },
    priceText: {
        fontFamily: MEDIUM,
        fontSize: fSize(14),
        lineHeight: 18,
        color: "#231100",
    },
    variantContainer: {
        marginTop: 2
    },
    variantItem: {
        marginBottom: 8,
    },
    extraContainer:{
        paddingTop: 15,
        flexDirection: 'row'
    },
})

export default CartItem;