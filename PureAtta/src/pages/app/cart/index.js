import React, { useState, useEffect } from "react";
import {StyleSheet, View, ScrollView, Image, Text, 
    TouchableOpacity, TextInput, FlatList, Alert} from "react-native";
import { useDispatch, useSelector } from "react-redux";

import Images from "../../../../assets/images/Images";
import { getCart, updateCartItem, deleteCartItem } from "../../../redux/actions/cartAction";
import { backNavigation } from "../../../root/navigation";
import { fSize, getHeight, getWidth } from "../../../utils/responsive";
import { ScreenNames } from "../../../utils/screens";
import { MEDIUM, REGULAR, SEMI_BOLD } from "../../../utils/typography";
import Header from "../components/header";
import CartItem from "./components/cartItem";

const Cart = (props) => {
    const { cartId } = props.route.params
    const dispatch = useDispatch(0)
    const { data } = useSelector(state=>state.cart)

    const headerLeft = () => (
        <TouchableOpacity style={styles.iconContainer} onPress={()=>{backNavigation()}}>
            <Image source={Images.arrowLeftBlackIcon} style={styles.icon}/>
        </TouchableOpacity>
    )

    const headerMiddle = () => (
        <Text style={styles.headerMiddleText}>{"Cart"}</Text>
    )


    const deleteItem = async(item) => {
        let formData = {
            _id: item?._id,
            cartId: data?._id
        }
        dispatch(deleteCartItem(formData))
    }

    useEffect(()=>{
        if(cartId){
            dispatch(getCart())
        }
    },[cartId])
    
    const onChangeCartItem = async (item, value) => {
        let formData = {
            _id:item?._id,
            productId: item?.product?._id,
            variantId: item?.variant?._id,
            quantity: value
        }
        dispatch(updateCartItem(formData))
    }

    return(
        <View style={styles.container}>
            <View style={styles.header}>
                <Header 
                    headerLeft={headerLeft}
                    headerMiddle={headerMiddle}
                    headerRight={null}
                />
            </View>
            {
                data && data?.items?.length > 0 ?
                <>
                    <View style={styles.content}>
                        <ScrollView contentContainerStyle={{height: '90%'}}>
                            <View style={{flexDirection: 'row'}}>
                            </View>
                            {
                                data?.items?.map((item)=>(
                                    <CartItem key={item?._id} item={item} updateItem={onChangeCartItem} deleteItem={deleteItem}/>
                                ))
                            }
                        </ScrollView>
                        <View style={styles.buttonContainer}>
                            <TouchableOpacity style={styles.button}
                                onPress={()=>{props.navigation.navigate(ScreenNames.CART_SUMMARY)}}
                            > 
                                <Text style={styles.buttonText}>{"Checkout"}</Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                </>
                :
                <View style={styles.emptyContainer}>
                    <View style={styles.emptyImageContainer}>
                        <Image source={Images.emptyImage} />
                    </View>
                    <View style={styles.emptyTextContainer}>
                        <Text style={styles.emptyText}>{"Your Cart is Empty."}</Text>
                    </View>
                    <View style={styles.buttonContainer}>
                        <TouchableOpacity style={styles.returnButton}>
                            <Text style={styles.returnButtonText}>{"Keep Shopping"}</Text>
                        </TouchableOpacity>
                    </View>
                </View>
            }
        </View>
    )
}

const styles = StyleSheet.create({
    container:{
        width: getWidth(100),
        height: getHeight(100),
        backgroundColor: "#F7F7F7",
    },
    header:{
        height: getHeight(10),
    },
    headerMiddleText:{
        fontFamily: REGULAR,
        fontSize: fSize(20),
        lineHeight: 28,
        color: "#1C1B1F",
        textAlign: 'left'
    },
    iconContainer:{
        padding: 10,
        justifyContent: 'center',
        alignItems: 'flex-start'
    },
    icon:{
    
    },
    content:{
        height: getHeight(90),
        padding: 20,    
    },
    buttonContainer:{
        width: "100%",
        height: "10%",
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: "#F7F7F7"
    },
    button:{
        width: "100%",
        height: 48,
        backgroundColor :"#F2921D",
        borderRadius: 100,
        justifyContent: 'center',
        alignItems: 'center'
    },
    buttonText:{
        fontFamily: MEDIUM,
        fontSize: fSize(16),
        lineHeight: 20,
        color: "#FFFFFF",
    },
    emptyContainer:{
        height: getHeight(90),
        justifyContent: 'center',
        paddingHorizontal: getWidth(10)
    },
    emptyImageContainer:{
        justifyContent: 'center',
        alignItems: 'center',
        marginBottom: 10
    },
    emptyTextContainer:{
        marginBottom: 10,
        justifyContent: 'center',
        alignItems: 'center'
    },
    emptyText:{
        fontFamily: MEDIUM,
        fontSize: fSize(16),
        lineHeight: 20,
        color: "#828282",
    },
    returnButton:{
        width: "90%",
        height: 48,
        backgroundColor :"rgba(242, 146, 29, 0.1)",
        borderRadius: 100,
        justifyContent: 'center',
        alignItems: 'center',
        borderWidth: 1,
        borderColor: "#F2921D"
    },
    returnButtonText:{
        fontFamily: MEDIUM,
        fontSize: fSize(16),
        lineHeight: 20,
        color: "#F2921D",
    },
})

export default Cart;