import React from 'react';
import { StyleSheet, View, Text, TextInput,  } from 'react-native'; 
import { reduxForm, Field} from 'redux-form';

import { fSize } from '../../../../../utils/responsive';
import { REGULAR, SEMI_BOLD } from '../../../../../utils/typography';


const validate = values => {    
    const errors = {
    }

    if (!values.firstName) {
        errors.firstName = 'first name must not be empty '
    }

    if (!values.lastName) {
        errors.lastName = 'last name  not be empty '
    }

    if (!values.email) {
        errors.email = 'email must not be empty'
    }

    if (values.email) {
        const re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        if(!re.test(String(values.email).toLowerCase())){
           errors.email = 'invalid email'
        }
    }
    return errors
}

const renderInput = ({ input, label, type, placeholder, disabled, meta: { touched, error, warning }}) => (
    <View style={styles.formItem}>
        <Text style={styles.formLabel}>{label}</Text>
        <TextInput
            {...input}
            style={[styles.formInput,{backgroundColor: disabled ? "lightgray" : "white"}]}
            placeholder={placeholder}
            placeholderTextColor={"color: rgba(0, 0, 0, 0.4)"}
            keyboardType={type}
            editable={disabled}
        />
        {   
         touched && error  && error !== 'email must not be empty' && error !== 'invalid email'  && input?.name =='email' && <Text style={{color:'#FF7373',paddingHorizontal:5}}>{error}</Text>
        }
        
    </View>
)

let UserForm = (props) => {
    const { handleSubmit, onSubmit} = props;
    return (
        <View>
            <Field
                name={"firstName"} 
                component={renderInput} 
                type={"default"} 
                label={"First Name"}
                placeholder={"Eg. John"}
            />
            <Field
                name={"lastName"} 
                component={renderInput} 
                type={"default"} 
                label={"Last Name"}
                placeholder={"Eg. Doe"}
            />
            <Field
                name={"email"} 
                component={renderInput} 
                type={"email-address"} 
                label={"Email"}
                placeholder={"Eg. example@gmail.com"}
            />
            <Field
                name={"phone"} 
                component={renderInput} 
                type={"phone-pad"} 
                label={"Mobile Number"}
                placeholder={"Eg. 8888888888"}
                disabled={true}
            />
        </View>
    )
}

UserForm = reduxForm({
    form: 'user-form',
    validate,
    shouldValidate: () => true,
    enableReinitialize: true
})(UserForm)


const styles = StyleSheet.create({
    formItem:{
        marginBottom: 20
    },
    formLabel:{
        fontFamily: SEMI_BOLD,
        fontSize: fSize(14),
        lineHeight: 21,
        color: "#231100",
        textAlign: 'left',
    },
    formInput:{
        marginTop: 10,
        backgroundColor: '#fff',
        borderWidth: 1,
        borderRadius: 3.5,
        borderColor: "#E5E5E5",
        height: 50,
        fontFamily: REGULAR,
        fontSize: fSize(14),
        lineHeight: 17,
        color: "#231100",
        padding: 15
    },
})

export default UserForm;