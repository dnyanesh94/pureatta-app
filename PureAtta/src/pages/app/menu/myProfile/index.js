import React, {useEffect} from "react";
import {StyleSheet, View, Text, ScrollView, 
    TouchableOpacity, Image} from 'react-native';
import { useDispatch, useSelector } from "react-redux";
import { submit, change, isPristine } from 'redux-form';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'

import Images from "../../../../../assets/images/Images";
import { fSize, getHeight, getWidth } from "../../../../utils/responsive";
import { REGULAR } from "../../../../utils/typography";
import PrimaryButton from "../components/primaryButton";
import UserForm from "./components/userForm";
import { updateUserData } from "../../../../redux/actions/userAction";


const MyProfile = (props) => {

    const dispatch = useDispatch()
    const state = useSelector(state=>state)
    const data = state.user.data
    const goBack = () => {
        props.navigation.goBack()
    }
   
    
    const headerLeft = () => (
        <TouchableOpacity style={styles.iconContainer} onPress={goBack}>
            <Image source={Images.arrowLeftBlackIcon} style={styles.icon}/>
        </TouchableOpacity>
    )

    const headerMiddle = () => (
        <Text style={styles.headerTitle}>{"Manage Profile"}</Text>
    )
    
    useEffect(()=>{
        if(data){
            dispatch(change('user-form', 'firstName', data?.firstName))
            dispatch(change('user-form', 'lastName', data?.lastName))
            dispatch(change('user-form', 'email', data?.email))
            dispatch(change('user-form', 'phone', data?.phone))
        }
    },[data])
    
    const onSubmit = (values) => {
        let formData = {
            _id: data?._id,
            ...values
        }
        dispatch(updateUserData(formData))    
    }

    return(
        <KeyboardAwareScrollView style={styles.container}>
            <View style={styles.container}>
                <View style={styles.header}>
                    <View style={{flex: 0.15}}>
                        {headerLeft()}
                    </View>
                    <View style={{flex: 0.85}}>
                        {headerMiddle()}
                    </View>
                </View>
                <ScrollView>
                    <View style={styles.content}>
                        <View style={styles.imageSection}>
                            <Image source={Images.userPlaceholder} />
                        </View>
                        <View style={styles.formSection}>
                            <UserForm onSubmit={onSubmit} />
                        </View>
                    </View>
                    <View style={styles.footer}>
                        {
                           true ?
                            <PrimaryButton title={"Update Details"} onPress={()=>{dispatch(submit('user-form'))}} />
                            :<>
                                <View style={{height: 10}}/>
                                <SecondaryButton title={"Update Details"} onPress={()=>{}} disabled={true}/>
                            </>
                        }
                    </View>
                </ScrollView>
            </View>
       </KeyboardAwareScrollView> 
    )
}

const styles = StyleSheet.create({
    container:{
        height: getHeight(100),
        width: getWidth(100),
        backgroundColor:"#F7F7F7",
    },
    header:{
        height: getHeight(10),
        flexDirection:'row',
        paddingHorizontal: 15,
        backgroundColor: '#FFFBFE',
        shadowColor: '#000000',
        shadowOffset: {
            width: 0,
            height: 2
        },
        shadowRadius: 2,
        shadowOpacity: 0.2,
        alignItems: 'center',
        paddingTop: 20
    },
    headerTitle:{
        fontFamily: REGULAR,
        fontSize: fSize(20),
        lineHeight: 28,
        color: "#1C1B1F",
        textAlign: 'left'
    },
    iconContainer:{
        padding: 10,
        justifyContent: 'center',
        alignItems: 'flex-start'
    },
    icon:{
    
    },
    content:{
        padding: 20,
        height: getHeight(70)
    },
    imageSection:{
        marginVertical: 10,
        paddingVertical: 10,
        justifyContent: 'center',
        alignItems: 'center'
    },
    formSection:{
        marginVertical: 10,
        paddingVertical: 10,
        justifyContent: 'center',
    },
    footer:{
        height: getHeight(15),
        justifyContent: 'flex-end',
        alignItems: 'center',
        // padding: 20,
    }

})

export default MyProfile;