import React, { useEffect, useState } from "react";
import {StyleSheet, Modal, View, Text, ScrollView, TouchableOpacity, Image} from 'react-native';
import { useDispatch, useSelector } from "react-redux";
import Images from "../../../../../assets/images/Images";
import PrimaryButton from "../../../../components/primaryButton";
import { addUserAddress, changeDefaultAddress, getUserAddressList } from "../../../../redux/actions/userAction";
import { fSize, getHeight, getWidth } from "../../../../utils/responsive";
import { REGULAR } from "../../../../utils/typography";
import AddressItem from "../../addressComponents/addressItem";
import AddressModal from "../../addressComponents/addressModal";

const ManageAddress = (props) => {
    const dispatch = useDispatch()
    const {addressList} = useSelector(state=>state.user)
    const [addressModal, setAddressModal] = useState(false)

    const goBack = () => {
        props.navigation.goBack()
    }

    const headerLeft = () => (
        <TouchableOpacity style={styles.iconContainer} onPress={goBack}>
            <Image source={Images.arrowLeftBlackIcon} style={styles.icon}/>
        </TouchableOpacity>
    )

    const headerMiddle = () => (
        <Text style={styles.headerTitle}>{"Manage Address"}</Text>
    )

    useEffect(()=>{
        dispatch(getUserAddressList())
    },[])

    const onChangeDefault = (address) => {
        dispatch(changeDefaultAddress({_id:address?._id, isDefault: true}))
    }

    const addAddress = (values) => {
        dispatch(addUserAddress(values))
        setAddressModal(false)
    }

    return(
        <View style={styles.container}>
            <View style={styles.header}>
                <View style={{flex: 0.15}}>
                    {headerLeft()}
                </View>
                <View style={{flex: 0.85}}>
                    {headerMiddle()}
                </View>
            </View>
            <ScrollView contentContainerStyle={styles.content}>
                {
                    addressList?.map(address=>(
                        <AddressItem 
                            key={address?._id} 
                            address={address}
                            onPress={onChangeDefault}
                        />
                    ))
                }
            </ScrollView>
            <View style={styles.footer}>
                <PrimaryButton title={"Add Address"} onPress={()=>{setAddressModal(true)}} />
            </View>
            {
                addressModal &&
                <AddressModal 
                    onClose={()=>{setAddressModal(false)}}
                    addressList={addressList}
                    addAddress={addAddress}
                />
            }
        </View>
    
    )
}

const styles = StyleSheet.create({
    container:{
        height: getHeight(100),
        width: getWidth(100),
        backgroundColor:"#F7F7F7",
    },
    header:{
        height: getHeight(10),
        flexDirection:'row',
        paddingHorizontal: 15,
        backgroundColor: '#FFFBFE',
        shadowColor: '#000000',
        shadowOffset: {
            width: 0,
            height: 2
        },
        shadowRadius: 2,
        shadowOpacity: 0.2,
        alignItems: 'center',
        paddingTop: 20
    },
    headerTitle:{
        fontFamily: REGULAR,
        fontSize: fSize(20),
        lineHeight: 28,
        color: "#1C1B1F",
        textAlign: 'left'
    },
    iconContainer:{
        padding: 10,
        justifyContent: 'center',
        alignItems: 'flex-start'
    },
    icon:{
    
    },
    content:{
        padding: 20,
        height: getHeight(75)
    },
    footer:{
        height: getHeight(10),
        justifyContent: 'flex-start',
        paddingHorizontal: 20,
    }

})

export default ManageAddress;