import React, { useState } from "react";
import {StyleSheet, Modal, View, Text, ScrollView, TouchableOpacity, Image, TextInput} from 'react-native';
import Images from "../../../../../assets/images/Images";
import { fSize, getHeight, getWidth } from "../../../../utils/responsive";
import { MEDIUM, REGULAR, SEMI_BOLD } from "../../../../utils/typography";

const Tabs = [{
    id: 0,
    title: "Ongoing Orders"
},{
    id: 1,
    title: "Completed Orders"
}]
const MyOrder = (props) => {
    
    const [activeTab, setActiveTab] = useState(0)
    const [orders, setOrders] = useState([])
    const { orderID } = props.route.params;

    const goBack = () => {
        props.navigation.goBack()
    }

    const headerLeft = () => (
        <TouchableOpacity style={styles.iconContainer} onPress={goBack}>
            <Image source={Images.arrowLeftBlackIcon} style={styles.icon}/>
        </TouchableOpacity>
    )

    const headerMiddle = () => (
        <Text style={styles.headerTitle}>{"Order Details"}</Text>
    )

    return(
        <View style={styles.container}>
            <View style={styles.header}>
                <View style={{flex: 0.15}}>
                    {headerLeft()}
                </View>
                <View style={{flex: 0.85}}>
                    {headerMiddle()}
                </View>
            </View>
            <View style={styles.content}>
                <ScrollView>
                    <View style={styles.section}>
                        <View style={styles.row}>
                            <Text style={styles.orderTitle}>{"MP Sihor Atta"}</Text>
                            <Text style={styles.orderPrice}>{"Rs. 200"}</Text>
                        </View>
                        <View style={[styles.row,{marginTop: 10}]}>
                            <Text style={styles.orderVariant}>{"3Kg, Normala"}</Text>
                        </View>    
                        <View style={[styles.row,styles.orderTrack]}>
                            <View style={styles.orderTrackPath}>
                                <View style={styles.statusActiveIcon}/>
                                <View style={styles.orderTrackLine}>
                                    {
                                        Array.from(Array(6), (_, i) => (
                                            <View 
                                                style={[
                                                    styles.orderTrackLineDash,
                                                    {backgroundColor: orderID == 0 && i > 2 ? '#E0E0E0' : '#66BD0F'}
                                                ]}
                                            />
                                        ))
                                    }
                                    
                                </View>
                                {
                                    orderID === 0 ?
                                    <View style={styles.statusInactiveIcon}/>
                                    :
                                    <View style={styles.statusActiveIcon}/>
                                }
                            </View>
                            <View style={styles.orderStatus}>
                                <View style={{flexDirection: 'column'}}>
                                    <Text style={styles.orderStatusText}>{"Ordered"}</Text>
                                    <Text style={styles.orderDate}>{"19th April 2022"}</Text>
                                </View>
                                <View style={{flexDirection: 'column'}}>
                                    <Text style={[styles.orderStatusText]}>{"Delivered"}</Text>
                                    <Text style={styles.orderDate}>{"19th April 2022"}</Text>
                                </View>
                            </View>
                        </View>
                    </View>
                    {
                        orderID !== 0 && 
                        <View style={styles.section}>
                            <View style={styles.row}>
                                <Text style={styles.ratingText}>
                                    {"Tell us How we are doing"}
                                </Text>
                            </View>
                            <View style={[styles.row,{justifyContent: 'flex-start', marginTop: 20}]}>
                                {
                                    Array.from(Array(5), (_, i) => (
                                        <TouchableOpacity style={{marginRight: 25}} key={i}>
                                            <Image source={Images.blackStar}/>
                                        </TouchableOpacity>
                                    ))
                                }
                            </View>
                        </View>
                    }
                    <View style={styles.section}>
                        <View style={[styles.row, styles.lightBorder]}>
                            <Text style={styles.addressHeading}>
                                {"Delivery Address"}
                            </Text>
                        </View> 
                        <View style={[styles.row,{marginTop: 5}]}>
                            <Text style={styles.nameText}>
                                {"Amol Gangawane"}
                            </Text>
                        </View>
                        <View style={[styles.row]}>
                            <Text style={styles.addressDetails}>
                                {"A2,708,Hadapsar,New colorny, Pune,412307"}
                            </Text>
                        </View>
                        <View style={[styles.row,{marginVertical: 5}]}>
                            <Text style={styles.phoneLabelText}>
                            Phone number: <Text style={styles.phoneValueText}>8888888888</Text>
                            </Text>
                        </View> 
                    </View>   
                    <View style={styles.section}>
                        <View style={[styles.row, styles.lightBorder]}>
                            <Text style={styles.addressHeading}>
                                {"Bill summary"}
                            </Text>
                        </View>
                        <View style={[styles.row, {paddingVertical: 10}]}>
                            <Text style={styles.billLabel}>{"Item Total"}</Text>
                            <Text style={styles.billValue}>{"Rs. 400"}</Text>
                        </View>
                        <View style={[styles.row, styles.darkBorder, {paddingVertical: 10}]}>
                            <Text style={styles.billLabel}>{"Delivery Charges"}</Text>
                            <Text style={[styles.billValue, {color:"#FF8A1E"}]}>{"Free"}</Text>
                        </View>
                        <View style={[styles.row, styles.darkBorder, {paddingVertical: 10}]}>
                            <Text style={styles.billLabel}>{"Total Amount"}</Text>
                            <Text style={styles.billValue}>{"Rs. 400"}</Text>
                        </View>
                        <View style={[styles.row, {paddingVertical: 10}]}>
                            <Text style={styles.billLabel}>{"Payment Mode"}</Text>
                            <Text style={[styles.billValue, {color:"#FF8A1E"}]}>{"COD"}</Text>
                        </View>
                    </View>   
                </ScrollView>
            </View>    
        </View>
    
    )
}

const styles = StyleSheet.create({
    container:{
        height: getHeight(100),
        width: getWidth(100),
        backgroundColor:"#F7F7F7",
    },
    header:{
        height: getHeight(10),
        flexDirection:'row',
        paddingHorizontal: 15,
        backgroundColor: '#FFFBFE',
        shadowColor: '#000000',
        shadowOffset: {
            width: 0,
            height: 2
        },
        shadowRadius: 2,
        shadowOpacity: 0.2,
        alignItems: 'center',
        paddingTop: 20
    },
    headerTitle:{
        fontFamily: REGULAR,
        fontSize: fSize(20),
        lineHeight: 28,
        color: "#1C1B1F",
        textAlign: 'left'
    },
    iconContainer:{
        padding: 10,
        justifyContent: 'center',
        alignItems: 'flex-start'
    },
    icon:{
    
    },
    content:{
        height: getHeight(85),
        paddingBottom: 20
    },
    section:{
        backgroundColor: "#FFFFFF",
        padding: 20,
        marginTop: 20  ,
    },
    row:{
        flexDirection: 'row',
        justifyContent: 'space-between',
        paddingVertical: 5
    },
    orderTitle:{
        fontFamily: MEDIUM,
        fontSize: fSize(14),
        lineHeight: 16,
        color: "#231100",
    },
    orderPrice:{
        fontFamily: MEDIUM,
        fontSize: fSize(14),
        lineHeight: 16,
        color: "#231100",
    },
    orderVariant:{
        fontFamily: REGULAR,
        fontSize: fSize(12),
        lineHeight: 16,
        color: "#828282",
    },
    orderTrack:{
        marginTop: 20, 
        height: 180,
    },
    orderTrackPath:{
        flex: 0.1,
        height: "100%",
        justifyContent: 'flex-start',
        alignItems: 'center'
    },
    statusActiveIcon:{
        height: 20, 
        width: 20, 
        backgroundColor: "#66BD0F", 
        borderRadius: 10
    },
    statusInactiveIcon:{
        height: 20, 
        width: 20, 
        backgroundColor: "#E0E0E0", 
        borderRadius: 10
    },
    orderTrackLine:{
        width: "100%",
        height: "65%",
        alignItems: 'center',
        justifyContent: 'center'
    },
    orderTrackLineDash:{
        backgroundColor:"#66BD0F", 
        height: "8%", 
        width: 4, 
        marginVertical: 4
    },
    orderStatus:{
        flex: 0.9,
        height: "100%",
        justifyContent: 'space-between',
        // paddingTop: 10
    },
    orderStatusText:{
        fontFamily: REGULAR,
        fontSize: fSize(14),
        lineHeight: 17,
        color: "#000000",
        paddingBottom: 5,
    },
    orderDate:{
        fontFamily: REGULAR,
        fontSize: fSize(14),
        lineHeight: 17,
        color: "#828282",
    },
    ratingText:{
        fontFamily: MEDIUM,
        fontSize: fSize(14),
        lineHeight: 16,
        color: "#000000",
    },
    addressHeading:{
        fontFamily: REGULAR,
        fontSize: fSize(14),
        lineHeight: 16,
        color: "#828282",
    },
    lightBorder:{
        borderBottomWidth: 0.2, 
        borderColor: "#828282",
    },
    nameText:{
        fontFamily: MEDIUM,
        fontSize: fSize(14),
        lineHeight: 16,
        color: "#000000",
    },
    addressDetails:{
        fontFamily: REGULAR,
        fontSize: fSize(14),
        lineHeight: 20,
        color: "#231100",
    },
    phoneLabelText:{
        fontFamily: MEDIUM,
        fontSize: fSize(14),
        lineHeight: 16,
        color: "#000000",
    },
    phoneValueText:{
        fontFamily: REGULAR,
    },
    billHeading:{
        fontFamily: REGULAR,
        fontSize: fSize(14),
        lineHeight: 16,
        color: "#828282",
    },
    billLabel:{
        fontFamily: REGULAR,
        fontSize: fSize(14),
        lineHeight: 17,
        color: "#231100",
    },
    billValue:{
        fontFamily: MEDIUM,
        fontSize: fSize(14),
        lineHeight: 16,
        color: "#231100",
    },
    darkBorder:{
        borderBottomWidth: 1, 
        borderColor: "#828282",
    },
})

export default MyOrder;