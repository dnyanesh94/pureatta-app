import React from 'react';
import {StyleSheet, View, Text, TouchableOpacity, Image} from 'react-native';
import Images from '../../../../../../assets/images/Images';
import { fSize } from '../../../../../utils/responsive';
import { ScreenNames } from '../../../../../utils/screens';
import { MEDIUM, REGULAR } from '../../../../../utils/typography';


const CompletedOrderItem = (props) => {
    return(
        <TouchableOpacity style={styles.itemContainer}
            onPress={()=>{props.navigation.navigate(ScreenNames.ORDER_DETAILS,{orderID:1})}}
        >
            <View style={styles.itemRow}>
                <Text style={styles.orderTitle}>{"MP Sihor Atta"}</Text>
                <Text style={styles.orderPrice}>{"Rs. 200"}</Text>
            </View>
            <View style={styles.itemRow}>
                <Text style={styles.orderVariant}>{"3Kg, Normala"}</Text>
                <TouchableOpacity style={{padding: 5, paddingRight: 0}}>
                    <Image source={Images.nextIcon} />
                </TouchableOpacity>
            </View>
            <View style={styles.itemRow}>
                <Text style={styles.orderTime}>{"Delivered on 20th April 2022"}</Text>
            </View>
            <View style={[styles.itemRow,{justifyContent: 'flex-start', marginTop: 20}]}>
                {
                    Array.from(Array(5), (_, i) => (
                        <TouchableOpacity style={{marginRight: 10}} key={i}>
                            <Image source={Images.blackStar}/>
                        </TouchableOpacity>
                    ))
                }
            </View>
            <View style={[styles.itemRow, {marginTop: 5}]}>
                <Text style={styles.ratingText}>{"Rate this product now !"}</Text>
            </View>  
        </TouchableOpacity>
    )
}

const styles = StyleSheet.create({
    itemContainer:{
        padding: 20,
        marginVertical: 15,
        backgroundColor: "#ffffff"
    },
    itemRow:{
        flexDirection: 'row',
        paddingBottom: 6,
        justifyContent: 'space-between'
    },
    orderTitle:{
        fontFamily: MEDIUM,
        fontSize: fSize(14),
        lineHeight: 16,
        color: "#231100",
    },
    orderPrice:{
        fontFamily: MEDIUM,
        fontSize: fSize(14),
        lineHeight: 16,
        color: "#231100",
    },
    orderVariant:{
        fontFamily: REGULAR,
        fontSize: fSize(12),
        lineHeight: 16,
        color: "#828282",
    },
    orderTime:{
        fontFamily: REGULAR,
        fontSize: fSize(14),
        lineHeight: 17,
        color: "#66BD0F",
    },
    ratingText:{
        fontFamily: REGULAR,
        fontSize: fSize(14),
        lineHeight: 17,
        color: "#828282",
    }

})

export default CompletedOrderItem;