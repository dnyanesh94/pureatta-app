import React, { useState } from "react";
import {StyleSheet, Modal, View, Text, ScrollView, TouchableOpacity, Image, TextInput} from 'react-native';
import Images from "../../../../../assets/images/Images";
import { fSize, getHeight, getWidth } from "../../../../utils/responsive";
import { MEDIUM, REGULAR, SEMI_BOLD } from "../../../../utils/typography";
import CompletedOrderItem from "./components/completedOrderItem";
import OngoingOrderItem from "./components/ongoingOrderItem";

const Tabs = [{
    id: 0,
    title: "Ongoing Orders"
},{
    id: 1,
    title: "Completed Orders"
}]
const MyOrder = (props) => {
    
    const [activeTab, setActiveTab] = useState(0)
    const [orders, setOrders] = useState([])

    const goBack = () => {
        props.navigation.goBack()
    }

    const headerLeft = () => (
        <TouchableOpacity style={styles.iconContainer} onPress={goBack}>
            <Image source={Images.arrowLeftBlackIcon} style={styles.icon}/>
        </TouchableOpacity>
    )

    const headerMiddle = () => (
        <Text style={styles.headerTitle}>{"My Orders"}</Text>
    )

    return(
        <View style={styles.container}>
            <View style={styles.header}>
                <View style={{flex: 0.15}}>
                    {headerLeft()}
                </View>
                <View style={{flex: 0.85}}>
                    {headerMiddle()}
                </View>
            </View>
            <View style={styles.content}>
                <View style={styles.tabContainer}>
                    {
                        Tabs.map(tab=>(
                            <TouchableOpacity 
                                key={tab.id}
                                style={[styles.tab, activeTab === tab.id && styles.activeBorder]} 
                                onPress={()=>{
                                    setActiveTab(tab.id)
                                }}
                            >
                                <Text style={[styles.tabTitle, activeTab === tab.id && styles.activeColor]}>{tab.title}</Text>
                            </TouchableOpacity>
                        ))
                    }
                </View>
                {
                    // orders.length === 0
                    //     ?
                    // <View style={styles.emptyContainer}>
                    //     <Image source={Images.noOrderImage} />
                    //     <Text style={styles.emptyText}>{"No orders found"}</Text>
                    // </View>
                    //     :
                    <ScrollView>
                        {
                            activeTab === 0 ?
                            <OngoingOrderItem {...props}/>
                            : <CompletedOrderItem {...props}/>
                        }
                    </ScrollView>
                }
            </View>    
        </View>
    
    )
}

const styles = StyleSheet.create({
    container:{
        height: getHeight(100),
        width: getWidth(100),
        backgroundColor:"#F7F7F7",
    },
    header:{
        height: getHeight(10),
        flexDirection:'row',
        paddingHorizontal: 15,
        backgroundColor: '#FFFBFE',
        shadowColor: '#000000',
        shadowOffset: {
            width: 0,
            height: 2
        },
        shadowRadius: 2,
        shadowOpacity: 0.2,
        alignItems: 'center',
        paddingTop: 20
    },
    headerTitle:{
        fontFamily: REGULAR,
        fontSize: fSize(20),
        lineHeight: 28,
        color: "#1C1B1F",
        textAlign: 'left'
    },
    iconContainer:{
        padding: 10,
        justifyContent: 'center',
        alignItems: 'flex-start'
    },
    icon:{
    
    },
    content:{
        height: getHeight(85)
    },
    tabContainer:{
        flexDirection: 'row',
        height: 50,
        backgroundColor: "#F7F7F7",
        shadowOffset: { width: 1, height: 1.4 },
        shadowOpacity:  0.4,
        shadowRadius: 1.0,
        elevation: 8,
        justifyContent: 'space-between'
    },
    tab:{
        height: "100%",
        justifyContent: 'center',
        alignItems: 'center',
        width: "50%",
       
    },
    tabTitle: {
        fontFamily: MEDIUM,
        fontSize: fSize(14),
        lineHeight: 16,
        color: "#231100",
    },
    activeBorder: {
        borderBottomWidth: 2,
        borderColor: "#FF8A1E"
    },
    activeColor:{
        color: "#FF8A1E"
    },
    emptyContainer:{
        height: "90%",
        justifyContent: 'center',
        alignItems: 'center'
    },
    emptyText:{
        marginTop: 20,
        fontFamily: REGULAR,
        fontSize: fSize(16),
        lineHeight: 20,
        color: "#828282",
    }
})

export default MyOrder;