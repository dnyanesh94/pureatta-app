import React, {useState} from 'react';
import { StyleSheet, View, Text, TextInput, TouchableOpacity, Image  } from 'react-native'; 
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import { reduxForm, Field} from 'redux-form';
import Images from '../../../../assets/images/Images';

import { fSize } from '../../../utils/responsive';
import { MEDIUM, REGULAR, SEMI_BOLD } from '../../../utils/typography';


const validate = values => {    
    const errors = {
    }

    if (!values.name) {
        errors.name = 'Name is required '
    }

    if (!values.address1) {
        errors.address1 = 'House No, Building Name is required '
    }

    if (!values.address2) {
        errors.address2 = 'Road Name, Area, Colony is required'
    }
    
    if (!values.city) {
        errors.city = 'City is required'
    }

    if (!values.pincode) {
        errors.pincode = 'Pincode is required'
    }

    if (!values.phone) {
        errors.phone = 'Phone number is required'
    }

    return errors
}

const renderInput = ({ input, label, type, placeholder, disabled, meta: { touched, error, warning }}) => (
    <View style={styles.formItem}>
        <Text style={styles.formLabel}>{label}</Text>
        <TextInput
            {...input}
            style={[styles.formInput,{backgroundColor: disabled ? "lightgray" : "white"}]}
            placeholder={placeholder}
            placeholderTextColor={"color: rgba(0, 0, 0, 0.4)"}
            keyboardType={type}
            editable={disabled}
        />
        {   
         touched && error  && error !== 'email must not be empty' && error !== 'invalid email'  && input?.name =='email' && <Text style={{color:'#FF7373',paddingHorizontal:5}}>{error}</Text>
        }
    </View>
)

const TOGGLE_BUTTONS = [
    {
        id: "HOME",
        name: "Home",
        icon: Images.homeIcon

    },
    {
        id: "OFFICE",
        name: "Office",
        icon: Images.suitcaseIcon
    }
]


let AddressForm = (props) => {
    const { handleSubmit, onSubmit, selectedType, setSelectedType} = props;

    return (
        <KeyboardAwareScrollView>
            <Field
                name={"name"} 
                component={renderInput} 
                type={"default"} 
                label={"Full Name*"}
                placeholder={"Eg.Jhon Doe"}
            />
            <Field
                name={"address1"} 
                component={renderInput} 
                type={"default"} 
                label={"House No, Building Name*"}
                placeholder={"Eg.A2, 708"}
            />
            <Field
                name={"address2"} 
                component={renderInput} 
                type={"default"} 
                label={"Road Name, Area, Colony*"}
                placeholder={"Eg.Hadapsar road,New colony"}
            />
            <View style={styles.addressRow}>
                <View style={styles.addressCol}>
                    <Field
                        name={"city"} 
                        component={renderInput} 
                        type={"default"} 
                        label={"City*"}
                        placeholder={"Eg. Pune"}
                    />
                </View>
                <View style={styles.addressCol}>
                    <Field
                        name={"pincode"} 
                        component={renderInput} 
                        type={"default"} 
                        label={"Pincode*"}
                        placeholder={"Eg. 411028"}
                    />
                </View>
            </View>
            <Field
                name={"phone"} 
                component={renderInput} 
                type={"phone-pad"} 
                label={"Mobile Number"}
                placeholder={"Eg. 8888888888"}
            />
            <View style={styles.addressTypeContainer}>
                <Text style={styles.addressTypeTitle}>{"Address Type"}</Text>
                <View style={styles.addressRow}>
                    {
                        TOGGLE_BUTTONS?.map(button=>(
                            <View style={styles.addressCol} key={button.id}>
                                <TouchableOpacity 
                                    style={[styles.toggleButton,
                                        {
                                            backgroundColor: selectedType === button.id ? "rgba(242, 146, 29, 0.15)" : "#fff",
                                            borderColor: selectedType === button.id ? "#F2921D"  : "#BDBDBD"
                                        }
                                    ]}
                                    onPress={()=>{setSelectedType(button?.id)}}
                                >
                                    <Image source={button.icon} />
                                    <Text style={
                                       [ styles.toggleButtonText,
                                       { color: selectedType === button.id ? "#F2921D" : "#231100"   }]
                                    }>
                                        {button?.name}
                                    </Text>
                                </TouchableOpacity>
                            </View>
                        ))
                    }
                </View>
            </View>
        </KeyboardAwareScrollView>
    )
}

AddressForm = reduxForm({
    form: 'address-form',
    validate,
    shouldValidate: () => true,
    enableReinitialize: true
})(AddressForm)


const styles = StyleSheet.create({
    formItem:{
        marginBottom: 20
    },
    formLabel:{
        fontFamily: MEDIUM,
        fontSize: fSize(14),
        lineHeight: 21,
        color: "#231100",
        textAlign: 'left',
    },
    formInput:{
        marginTop: 10,
        backgroundColor: '#fff',
        borderWidth: 1,
        borderRadius: 3.5,
        borderColor: "#E5E5E5",
        height: 50,
        fontFamily: REGULAR,
        fontSize: fSize(14),
        lineHeight: 17,
        color: "#231100",
        padding: 15
    },
    addressRow:{
        width:"100%", 
        flexDirection: 'row', 
        justifyContent: 'space-between'
    },
    addressCol:{
        width:"48%",
        flexDirection: 'column'
    },
    addressTypeContainer:{
        flexDirection: 'column',
        marginBottom: 5
    },
    addressTypeTitle:{
        fontFamily: MEDIUM,
        fontSize: fSize(14),
        lineHeight: 21,
        color: "#231100",
        marginBottom: 8
    },
    toggleButton:{
        flexDirection: 'row',
        paddingHorizontal: 16,
        paddingVertical: 8,
        borderWidth: 0.5,
        borderRadius: 100,
        justifyContent: 'center',
        alignItems: 'center',
    },
    toggleButtonText:{
        fontFamily: REGULAR,
        fontSize: fSize(14),
        lineHeight: 17,
        marginTop: 4,
        marginLeft: 5
    },
})

export default AddressForm;