import React, { useState } from "react";
import {StyleSheet, Modal, View, Text, ScrollView, TouchableOpacity, Image} from 'react-native';
import { useDispatch, useSelector } from "react-redux";
import Images from "../../../../assets/images/Images";
import { addUserAddress } from "../../../redux/actions/userAction";
import { fSize, getHeight, getWidth } from "../../../utils/responsive";
import { REGULAR } from "../../../utils/typography";
import AddressItem from "./addressItem";
import AddressModal from "./addressModal";
import ButtonPrimary from "./buttonPrimary";

const AddressListModal = ({onClose, onPress, addressList, deliveryAddress}) => {
    const [addressModal, setAddressModal] = useState(false)
    const dispatch = useDispatch()
    
    const headerLeft = () => (
        <TouchableOpacity style={styles.iconContainer} onPress={onClose}>
            <Image source={Images.closeIcon} style={styles.icon}/>
        </TouchableOpacity>
    )

    const headerMiddle = () => (
        <Text style={styles.headerMiddleText}>{"Select Delivery Address"}</Text>
    )

    const addAddress = (values) => {
        dispatch(addUserAddress(values))
        setAddressModal(false)
    }

    return(
        <Modal
            animationType="slide"
            transparent={true}
            visible={true}
            onRequestClose={() => {
                Alert.alert("Modal has been closed.");
                onClose();
            }}
        >
            <View style={styles.modalContainer}>
                <View style={styles.modalHeader}>
                    <View style={{flex: 0.1}}>
                        {headerLeft()}
                    </View>
                    <View style={{flex: 0.9}}>
                        {headerMiddle()}
                    </View>
                </View>
                <ScrollView contentContainerStyle={styles.modalContent}>
                {
                    addressList?.map(address=>(
                        <AddressItem 
                            key={address?._id} 
                            address={address}
                            onPress={onPress}
                            deliveryAddress={deliveryAddress}
                        />
                    ))
                }
                </ScrollView>
                <View style={styles.modalFooter}>
                    <ButtonPrimary title={"Add Address"} onPress={()=>{setAddressModal(true)}} />
                </View>
            </View>
            {
                addressModal &&
                <AddressModal 
                    onClose={()=>{setAddressModal(false)}}
                    addAddress={addAddress}
                />
            }
        </Modal>
    )
}

const styles = StyleSheet.create({
    modalContainer:{
        height: getHeight(100),
        width: getWidth(100),
        backgroundColor:"#F7F7F7",
    },
    modalHeader:{
        height: getHeight(10),
        flexDirection:'row',
        paddingHorizontal: 15,
        backgroundColor: '#FFFBFE',
        shadowColor: '#000000',
        shadowOffset: {
            width: 0,
            height: 2
        },
        shadowRadius: 2,
        shadowOpacity: 0.2,
        alignItems: 'center',
        paddingTop: 20
    },
    headerMiddleText:{
        fontFamily: REGULAR,
        fontSize: fSize(20),
        lineHeight: 28,
        color: "#1C1B1F",
        textAlign: 'left'
    },
    iconContainer:{
        padding: 10,
        justifyContent: 'center',
        alignItems: 'flex-start'
    },
    icon:{
    
    },
    modalContent:{
        padding: 20,
        height: getHeight(75)
    },
    modalFooter:{
        height: getHeight(10),
        justifyContent: 'flex-start',
        paddingHorizontal: 20,
    }

})

export default AddressListModal;