import React, {useState} from 'react';
import { StyleSheet, View, Text, TouchableOpacity  } from 'react-native';
import { useDispatch, useSelector } from 'react-redux';
import { setDeliveryAddress } from '../../../redux/actions/cartSummaryAction';
import { changeDefaultAddress } from '../../../redux/actions/userAction';
import { fSize } from '../../../utils/responsive';
import { MEDIUM, REGULAR, SEMI_BOLD } from '../../../utils/typography';

const AddressItem = ({address,  onPress, deliveryAddress}) => {
   
    const IsActive = (address) => {
        if(deliveryAddress){
            return  deliveryAddress?._id === address?._id 
        }else{
           return address?.isDefault
        }
    }

    return(
        <View style={styles.itemContainer}>
            <View style={styles.itemLeft}>
                <TouchableOpacity style={[
                        styles.selectButtonContainer,
                        {borderColor: IsActive(address) ? "#FF8A1E" : "#BDBDBD" }
                    ]}
                    onPress={()=>{onPress(address)}}
                >
                    <View style={[
                        styles.selectButton,
                        {backgroundColor: IsActive(address) ? "#FF8A1E" : "#BDBDBD"}
                    ]}/>
                </TouchableOpacity>
            </View>
            <View style={styles.itemRight}>
                <View style={styles.addressContainer}>
                    <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
                        <Text style={styles.addressName}>{address?.name}</Text>
                    </View>
                    <Text style={styles.addressType}>
                        {address?.type}-{" "}
                        {
                            address?.phone &&
                            <Text style={styles.phoneText}>{address?.phone}</Text>
                        },
                    </Text>
                    <Text style={styles.fullAddress}>
                        {`${address?.address1}, ${address?.address2} ${address?.city}-${address?.pincode}`}
                    </Text>
                </View>
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    itemContainer:{
        flexDirection: 'row',
        padding: 10,
        backgroundColor: '#fff',
        borderRadius: 4,
        marginBottom: 10
    },
    itemLeft:{
        flex: 0.1,
    },
    itemRight:{
        flex: 0.9,
        justifyContent: 'center',
        alignItems: 'flex-start'
    },
    addressContainer:{
        width: "100%",
        flexDirection: 'column'
    },
    addressName:{
        fontFamily: SEMI_BOLD,
        fontSize: fSize(14),
        lineHeight: 17,
        color: "#000000",
        textAlign: 'left',
        marginBottom:10,
    },
    addressType:{
        fontFamily: REGULAR,
        fontSize: fSize(12),
        lineHeight: 15,
        color: "#FF8A1E",
        textAlign: 'left',
        marginBottom:5,
    },
    phoneText:{
        fontFamily: MEDIUM,
        fontSize: fSize(12),
        lineHeight: 15,
        color: "#828282",
        textAlign: 'left',
        paddingBottom: 5
    },
    fullAddress:{
        fontFamily: REGULAR,
        fontSize: fSize(14),
        lineHeight: 17,
        color: "#828282",
        textAlign: 'left',
        paddingBottom: 10
    },
    selectButtonContainer:{
        height: 25,
        width: 25,
        borderRadius: 12,
        borderWidth:2,
        borderColor: "#FF8A1E",
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: -2
    },
    selectButton:{
        height: 25,
        width: 25,
        borderRadius: 12,
        borderWidth:2,
    },
    selectButton:{
        height: 15,
        width: 15,
        borderRadius: 8,
    }
})

export default AddressItem;