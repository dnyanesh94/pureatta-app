import React from 'react';
import {View, StyleSheet, TouchableOpacity, Text} from 'react-native';
import { fSize } from '../../../utils/responsive';
import { MEDIUM } from '../../../utils/typography';

const ButtonSecondary = ({onPress, title}) => (
    <TouchableOpacity style={styles.button} onPress={onPress}>
        <Text style={styles.buttonText}>{title}</Text>
    </TouchableOpacity>
)

const styles = StyleSheet.create({
    button:{
        width: "100%",
        height: 48,
        backgroundColor :"rgba(31, 31, 31, 0.12)",
        borderRadius: 100,
        justifyContent: 'center',
        alignItems: 'center'
    },
    buttonText:{
        fontFamily: MEDIUM,
        fontSize: fSize(16),
        lineHeight: 20,
        color: "#828282",
    }
})

export default ButtonSecondary;