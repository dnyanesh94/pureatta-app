import React,{ useState } from "react";
import {StyleSheet, Modal, View, Text, TextInput, ScrollView, TouchableOpacity, Image} from 'react-native';
import { useDispatch } from "react-redux";
import { submit } from "redux-form";

import Images from "../../../../assets/images/Images";
import { fSize, getHeight, getWidth } from "../../../utils/responsive";
import { MEDIUM, REGULAR } from "../../../utils/typography";
import AddressForm from "./addressForm";
import ButtonPrimary from "./buttonPrimary";
import ButtonSecondary from "./buttonSecondary";

const AddressModal = ({onClose, addAddress}) => {
    const [selectedType, setSelectedType] = useState('HOME')
    const dispatch = useDispatch()

    const headerLeft = () => (
        <TouchableOpacity style={styles.iconContainer} onPress={onClose}>
            <Image source={Images.closeIcon} style={styles.icon}/>
        </TouchableOpacity>
    )

    const headerMiddle = () => (
        <Text style={styles.headerMiddleText}>{"Add Delivery Address"}</Text>
    )

    return(
        <Modal
            animationType="slide"
            transparent={true}
            visible={true}
            onRequestClose={() => {
                Alert.alert("Modal has been closed.");
                onClose();
            }}
        >   
            <View style={styles.modalHeader}>
                <View style={{flex: 0.1}}>
                    {headerLeft()}
                </View>
                <View style={{flex: 0.9}}>
                    {headerMiddle()}
                </View>
            </View>
            <ScrollView style={styles.modalContainer}>
                <View style={styles.noteContainer}>
                    <Text style={styles.noteText}>{"A detailed Address will help us reach your doorstep easily"}</Text>
                </View>
                <View style={styles.addressContainer}>
                    <AddressForm 
                        onSubmit={(values)=>{addAddress({...values, selectedType})}}
                        selectedType={selectedType}
                        setSelectedType={setSelectedType}
                    />
                </View>
                <View style={styles.buttonContainer}>
                    <ButtonPrimary 
                        title={"Save Address"}
                        onPress={()=>{dispatch(submit('address-form'))}}
                    />
                </View>
            </ScrollView>
        </Modal>
    )
}

const styles = StyleSheet.create({
    modalContainer:{
        height: getHeight(100),
        width: getWidth(100),
        backgroundColor:"#F7F7F7",
        padding: 20,
        paddingBottom: 100
    },
    modalHeader:{
        height: getHeight(10),
        flexDirection:'row',
        paddingHorizontal: 15,
        backgroundColor: '#FFFBFE',
        shadowColor: '#000000',
        shadowOffset: {
            width: 0,
            height: 2
        },
        shadowRadius: 2,
        shadowOpacity: 0.2,
        alignItems: 'center',
        paddingTop: 20,
    },
    headerMiddleText:{
        fontFamily: REGULAR,
        fontSize: fSize(20),
        lineHeight: 28,
        color: "#1C1B1F",
        textAlign: 'left'
    },
    iconContainer:{
        padding: 10,
        justifyContent: 'center',
        alignItems: 'flex-start'
    },
    icon:{
    
    },
    noteContainer:{
        paddingHorizontal: 16,
        paddingVertical: 5,
        backgroundColor: "rgba(242, 146, 29, 0.1)",
        borderRadius: 8,
        marginBottom: 20,
    },
    noteText:{
        fontFamily: REGULAR,
        fontSize: fSize(12),
        lineHeight: 16,
        color: "#FF8A1E",
    },
    addressContainer:{
        paddingHorizontal: 8,
        paddingVertical: 16,
        backgroundColor: "#FFFFFF",
        borderRadius: 8
    },
    buttonContainer:{
        width: "100%",
        paddingHorizontal: 20,
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: 20,
    }
})

export default AddressModal;