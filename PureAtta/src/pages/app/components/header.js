import React from 'react';
import {StyleSheet, View, Text, Image, TouchableOpacity} from 'react-native';
import { SafeAreaView } from 'react-native-safe-area-context';
import Images from '../../../../assets/images/Images';
import { fSize } from '../../../utils/responsive';
import { REGULAR, SEMI_BOLD } from '../../../utils/typography';

const Header = (props) => {
    const { headerLeft, headerMiddle, headerRight } = props;

    return(
        <View style={styles.container}>
            <View style={styles.containerLeft}>
                {headerLeft && headerLeft()}
            </View>
            <View style={styles.containerMiddle}>
                {headerMiddle && headerMiddle()}
            </View>
            <View style={styles.containerRight}>
                {headerRight && headerRight()}
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    container:{
        flexDirection:'row',
        height: '100%',
        paddingHorizontal: 10,
        backgroundColor: '#FFFBFE',
        shadowColor: '#000000',
        shadowOffset: {
            width: 0,
            height: 2
        },
        shadowRadius: 2,
        shadowOpacity: 0.2,
        alignItems: 'center',
        paddingTop: 20
    },
    containerLeft:{
        width:"15%",
    },
    containerMiddle:{
        width:"70%",
    },
    containerRight:{
        width:"15%",
    }
})

export default Header;