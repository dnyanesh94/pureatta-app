import React, {useState} from 'react';

import { View, Text, Image, TouchableOpacity, StyleSheet } from 'react-native';
import Images from '../../../../assets/images/Images';
import { API_URL } from '../../../config';
import { navigate } from '../../../root/navigation';
import { filepathExtractor } from '../../../utils/images';
import { fSize } from '../../../utils/responsive';
import { ScreenNames } from '../../../utils/screens';
import { MEDIUM, REGULAR, SEMI_BOLD } from '../../../utils/typography';




const ProductItem = ({item}) => {
    const sortedItems = item?.variants?.sort(function (a, b) {
        return Number(a.price)  - Number(b.price)
    })

    const image = item?.images?.[0] //sortedItems?.[sortedItems?.length-1].images ? sortedItems?.[sortedItems?.length-1].images : item?.images?.[0]  
    
    const onClickProduct = (productId) => {
        navigate(ScreenNames.PRODUCT_DETAILS,{productId:productId})
    }

    console.log(item)

    return(
        <TouchableOpacity style={[styles.productItem,{zIndex: 100}]} onPress={()=>{onClickProduct(item?._id)}}>
            <View style={styles.productItemLeft}>
                <Image  source={{uri:filepathExtractor(image?.url)}} style={styles.productItemLeftImage} />
            </View>
            <View style={styles.productItemRight}>
                <View style={styles.productNameContainer}>
                    <Text style={styles.productName}>{item.name}</Text>
                </View>
                <View style={{flexDirection:'row', marginTop: 10}}>
                    {   
                        item?.rating &&
                        Array.from(Array(Number(item?.rating)), (_, i) => (
                            <TouchableOpacity style={{marginRight: 10}} key={i}>
                                <Image source={Images.starIcon}/>
                            </TouchableOpacity>
                        ))
                    }
                </View>
                <View style={{flexDirection:'row', marginTop: 10}}>
                    <Text style={styles.variantText}>{sortedItems?.[sortedItems?.length-1]?.name}</Text>
                </View>
                <View style={styles.priceContainer}>
                    <Text style={styles.priceText}>Rs.{sortedItems?.[0]?.price} - Rs.{sortedItems?.[sortedItems?.length-1]?.price}</Text>
                </View>
            </View>     
         </TouchableOpacity>
    )
}

const styles = StyleSheet.create({
    productItem:{
        width: "100%",
        flexDirection: 'row',
        backgroundColor: '#ffffff',
        marginVertical: 10,
        padding: 20,
        borderRadius: 8,
        shadowColor: '#000',
        shadowOffset: { width: 0, height: 0.2 },
        shadowOpacity:  0.4,
        shadowRadius: 3,
        elevation: 5,
        overflow: 'visible',
    },
    productItemLeft:{
        width:  "30%",
        borderRadius: 8,
    },
    productItemLeftImage:{
        height: 100,
        width: 100
    },
    productItemRight:{
        width:"70%",
        paddingLeft: 30, 
    },
    productNameContainer:{
        flexDirection: 'row',  
    },
    productName:{
        fontFamily: SEMI_BOLD,
        fontSize: fSize(14),
        lineHeight: 17,
        color: "#231100",
    },
    variantText:{
        fontFamily: REGULAR,
        fontSize: fSize(14),
        lineHeight: 17,
        color: "#231100"
    },
    priceContainer:{
        flexDirection:'row',
        marginTop: 10,
    },
    priceText:{
        fontFamily: MEDIUM,
        fontSize: fSize(16),
        lineHeight: 20,
        color: "#231100",
    },
    variantContainer:{
        marginTop: 8
    },
    variantItem:{
        marginBottom: 8,
    },
    buttonContainer:{
    },
    addToCartButton:{
        backgroundColor: '#F2921D',
        borderRadius: 100,
        height: 40,
        justifyContent: 'center',
        alignItems: 'center'  
    },
    btnText:{
        fontFamily: MEDIUM,
        fontSize: fSize(14),
        lineHeight: 17,
        color: "#FFFFFF",
    }
})

export default ProductItem;