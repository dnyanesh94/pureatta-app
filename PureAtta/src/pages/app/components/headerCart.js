
import React, {useEffect, useState} from 'react';
import { StyleSheet, TouchableOpacity, Text, Image, View } from 'react-native';
import { useIsFocused } from "@react-navigation/native"

import Images from '../../../../assets/images/Images';
import { navigate } from "../../../root/navigation"
import { fSize } from '../../../utils/responsive';
import { ScreenNames } from "../../../utils/screens"
import { REGULAR } from '../../../utils/typography';
import { useSelector, useDispatch } from 'react-redux';
import { getCart } from  '../../../redux/actions/cartAction'

const HeaderCart = (props) => {
    const { data } = useSelector(state=>state.cart)
    const isFocused = useIsFocused()

    const dispatch = useDispatch()

    useEffect(()=>{
        dispatch(getCart())
    },[isFocused])


    return(
        <TouchableOpacity style={styles.iconContainer}
            onPress={()=>{navigate(ScreenNames.CART, {cartId: data?._id})}}
        >
            {
                data &&
                <View style={styles.countContainer}>
                    <Text style={styles.conuterText}>
                        {data ? data?.count : '0'}
                    </Text>
                </View>
            }
            <Image source={Images.cartIcon} style={styles.icon}/>

        </TouchableOpacity>
    )
}

const styles = StyleSheet.create({
    iconContainer:{
        padding: 10,
        justifyContent: 'center',
        alignItems: 'center'
    },
    icon:{

    },
    countContainer:{
        position: 'absolute',
        width: 25,
        height: 25,
        borderRadius: 12,
        backgroundColor: "#F2921D",
        top: 5,
        right: 5,
        justifyContent: 'center',
        alignItems: 'center',
        zIndex: 111
    },
    conuterText:{
        fontFamily: REGULAR,
        fontSize: fSize(12),
        lineHeight: 16,
        color: "#fff",
    }
})

export default HeaderCart;