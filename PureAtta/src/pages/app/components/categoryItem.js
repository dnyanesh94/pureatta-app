import React from 'react';
import {  StyleSheet, TouchableOpacity, Image, Text } from 'react-native';
import { filepathExtractor } from '../../../utils/images';
import { fSize } from '../../../utils/responsive';
import { SEMI_BOLD } from '../../../utils/typography';

const CategoryItem = ({item}) => {
    return(
        <TouchableOpacity  style={styles.categoryItem}>
            <Image  source={{uri:filepathExtractor(item?.image?.url)}} style={styles.categoryItemImage} />
            <Text style={styles.categoryItemTitle}>{item.name}</Text>
        </TouchableOpacity>
    )
}

const styles = StyleSheet.create({
    categoryItem: {
        width: "28%",
        justifyContent: 'center',
        alignItems: 'center',
        width: 125,
        height: 125,
    },
    categoryItemImage:{
        borderRadius:50,
        height: 100,
        width: 100,
        borderWidth: 4,
        borderColor: "#f2cb9b"
    },
    categoryItemTitle:{
        fontFamily: SEMI_BOLD,
        fontSize: fSize(14),
        lineHeight: 17,
        color: "#231100",
        textAlign: 'center',
        marginTop: 10
    }
})

export default CategoryItem;