import AsyncStorage from '@react-native-async-storage/async-storage';
import React, { useState, useEffect } from 'react';
import { StyleSheet, View, Text, TouchableOpacity, Modal, 
    Image, ScrollView, TouchableWithoutFeedback 
} from 'react-native';
import { useDispatch, useSelector } from 'react-redux';

import Images from '../../../../assets/images/Images';
import { navigate } from '../../../root/navigation';
import { fSize, getHeight, getWidth } from '../../../utils/responsive';
import { ScreenNames } from '../../../utils/screens';
import { REGULAR } from '../../../utils/typography';
import { getUserData } from '../../../redux/actions/userAction';

const MENUS = [
    {   
        key: 1,
        title: "My Profile",
        icon: Images.userIcon,
        route: ScreenNames.MY_PROFILE
    },
    {   
        key: 2,
        title: "Manage Addresses",
        icon: Images.locationIcon,
        route: ScreenNames.MANAGE_ADDRESS
    },
    {   
        key: 3,
        title: "My Orders",
        icon: Images.orderIcon,
        route: ScreenNames.MY_ORDER
    },
    {   
        key: 4,
        title: "Logout",
        icon: Images.orderIcon,
        route: ''
    }
]

const Sidebar = ({onClose}) => {
    const { data } = useSelector(state=>state.user)
    const dispatch = useDispatch()
    const onPressItem = async (item) => {
        onClose()
        if(item?.key !== 4) {
            navigate(item?.route)
        }else{
            await AsyncStorage.removeItem("token")
            let token  = AsyncStorage.getItem('token')
            if(!token){
                navigate(ScreenNames.PHONE_LOGIN)
            }
        }
    }

    useEffect(()=>{
        dispatch(getUserData())
    },[])

    return (
        <Modal
            animationType='none'
            transparent={true}
            visible={true}
            onRequestClose={() => {onClose()}}
        >
            <TouchableOpacity
                style={styles.container}
                activeOpacity={1}
                onPressOut={() => {onClose()}}
            >
                <ScrollView
                    directionalLockEnabled={true}
                    contentContainerStyle={styles.scrollModal}
                    scrollEnabled={false}
                >
                    <TouchableWithoutFeedback>
                        <View style={styles.modalContainer}>
                            <View style={styles.modalHeader}>
                                <View style={styles.modalHeaderLeft}>
                                    <Image source={Images.userPlaceholder} style={styles.userIcon}/>
                                </View>
                                <View style={styles.modalHeaderRight}>
                                    <Text style={styles.usernameLabel}>{data?.firstName+ ' ' + data?.lastName}</Text>
                                    <Text style={styles.username}>{data?.phone}</Text>
                                </View>
                            </View>
                            <View style={styles.modalContent}>
                                {
                                    MENUS.map((menuItem, index)=>(
                                        <TouchableOpacity 
                                            style={styles.menuItem} 
                                            key={index} 
                                            onPress={()=>{
                                                onPressItem(menuItem)
                                            }}>
                                            <View style={styles.menuItemLeft}>
                                                <Image source={menuItem.icon} />
                                            </View>
                                            <View style={styles.menuItemRight}>
                                               <Text style={styles.menuItemTitle} >{menuItem.title}</Text>
                                            </View>
                                        </TouchableOpacity>
                                    ))
                                }
                            </View>
                        </View>
                    </TouchableWithoutFeedback>
                </ScrollView>
            </TouchableOpacity>
        </Modal> 
    )
}
    
const styles = StyleSheet.create({
    container: {
        width: getWidth(100),
        height: getHeight(100),
        backgroundColor: "rgba(0,0,0,0.4)"
    },
    modalContainer:{
        width: getWidth(75),
        height: getHeight(100),
        backgroundColor: "#E5E5E5",
    },
    modalHeader:{
        height: "15%",
        borderBottomWidth: 1,
        borderColor: "#000000",
        flexDirection: 'row',
        paddingTop: 20,
        paddingHorizontal: 20
    },
    modalHeaderLeft:{
        flex: 0.3,
        justifyContent:  'center',
        alignItems: 'center'
    },
    userIcon:{
        height: 60,
        aspectRatio: 1
    },
    modalHeaderRight:{
        flex: 0.7,
        justifyContent: 'center'
    },
    usernameLabel:{
        fontFamily: REGULAR,
        fontSize: fSize(14),
        lineHeight: 17,
        color: "#828282",
        marginBottom: 10
    },
    username:{
        fontFamily: REGULAR,
        fontSize: fSize(14),
        lineHeight: 17,
        color: "#231100",
    },
    modalContent:{
        height: "85%",
    },
    menuItem:{
        borderBottomWidth: 0.2,
        borderBottomColor: "#828282",
        paddingVertical: 16,
        paddingHorizontal: 8,
        flexDirection: 'row'
    },
    menuItemLeft: {
        flex: 0.2,
        justifyContent: 'center',
        alignItems: 'center'
    },
    menuItemRight:{
        flex: 0.8,
        justifyContent: 'center',
    },
    menuItemTitle: {
        fontFamily: REGULAR,
        fontSize: fSize(14),
        lineHeight: 16,
        color: "#231100",
        textAlign: 'left'
    }
})

export default Sidebar;