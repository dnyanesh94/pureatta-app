const Images = {
    logo: require('./logo.png'),
    arrowLeftIcon: require('./arrowLeftIcon.png'),
    sideMenuIcon: require('./sideMenuIcon.png'),
    cartIcon: require('./cartIcon.png'),
    markerIcon: require('./markerIcon.png'),
    searchIcon: require('./searchIcon.png'),
    banner: require('./banner.png'),
    category: require('./category.png'),
    categoryIcon: require('./categoryIcon.png'),
    popularProductIcon: require('./popularProductIcon.png'),
    arrowLeftBlackIcon: require('./arrowLeftBlackIcon.png'),
    starIcon: require('./starIcon.png'),
    leafIcon: require('./leafIcon.png'),
    heartIcon: require('./heartIcon.png'),
    dropIcon: require('./dropIcon.png'),
    deleteIcon: require('./deleteIcon.png'),
    locationIcon: require('./locationIcon.png'),
    addIcon: require('./addIcon.png'),
    homeIcon: require('./homeIcon.png'),
    suitcaseIcon: require('./suitcaseIcon.png'),
    emptyImage: require('./emptyImage.png'),
    dropdownIcon: require('./dropdownIcon.png'),
    closeIcon: require('./closeIcon.png'),
    userPlaceholder: require('./userPlaceholder.png'),
    userIcon: require('./userIcon.png'),
    orderIcon: require('./orderIcon.png'),
    noOrderImage: require('./noOrderImage.png'),
    nextIcon: require('./nextIcon.png'),
    blackStar: require('./starBlackIcon.png'),
    orderSuccess: require('./orderSuccess.png')

}


export default Images;